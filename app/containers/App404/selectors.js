import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the page404 state domain
 */

const selectPage404Domain = state => state.get('page404', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by Page404
 */

const makeSelectPage404 = () =>
  createSelector(selectPage404Domain, substate => substate.toJS());

export default makeSelectPage404;
export { selectPage404Domain };
