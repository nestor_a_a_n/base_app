import { fromJS } from 'immutable';
import page404Reducer from '../reducer';

describe('page404Reducer', () => {
  it('returns the initial state', () => {
    expect(page404Reducer(undefined, {})).toEqual(fromJS({}));
  });
});
