/*
 * Page404 Messages
 *
 * This contains all the text for the Page404 component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  problem_title: {
    id: 'containers.app.404.problem_title',
    defaultMessage: "Oops! You're lost.",
  },
  problem_paragraph: {
    id: 'containers.app.404.problem_paragraph',
    defaultMessage: "The page you are looking for was not found.",
  },
  search: {
    id: 'containers.app.404.search',
    defaultMessage: "Search",
  },
});
