/*
 * AppReset Messages
 *
 * This contains all the text for the AppReset component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'app.containers.AppReset.title',
    defaultMessage: 'Reiniciar',
  },
  paragraph: {
    id: 'app.containers.AppReset.paragraph',
    defaultMessage: 'Recuperar tu contraseña',
  },
  reset_password: {
    id: 'app.containers.AppReset.reset_password',
    defaultMessage: 'Reiniciar contraseña',
  },
  success_title: {
    id: 'app.containers.AppReset.success_title',
    defaultMessage: 'Un paso mas',
  },
  success_paragraph: {
    id: 'app.containers.AppReset.success_paragraph',
    defaultMessage: 'Revisa tu correo y sigue las instrucciones para reiniciar tu contraseña.',
  },
});
