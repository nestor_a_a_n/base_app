import { fromJS } from 'immutable';
import pageResetReducer from '../reducer';

describe('pageResetReducer', () => {
  it('returns the initial state', () => {
    expect(pageResetReducer(undefined, {})).toEqual(fromJS({}));
  });
});
