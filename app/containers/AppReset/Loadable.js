/**
 *
 * Asynchronously loads the component for PageReset
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
