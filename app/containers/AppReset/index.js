/**
 *
 * AppReset
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Container, Row, Col, Card, CardBlock, Button, Input, InputGroup, InputGroupAddon } from 'reactstrap';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
// messages
import messages from './messages';

// Selectors
import makeSelectAppReset from './selectors';

// Actions
import { clearAction, changePropAction, attempAction } from './actions';

// Images
// import ImageChronosLogo from '../../images/chronos-app-logo.png';

// Custom
import Alert from '../../components/Alert';
import LoadingSpiner from '../../components/LoadingSpiner';

// Utils
import autoFocus from '../../utils/miscellaneous/autoFocus';
import data from '../../utils/miscellaneous/data';
import { URL_RESET_VALIDATE } from '../../utils/constants';

/* eslint-disable react/prefer-stateless-function */
export class AppReset extends React.PureComponent {
  constructor(props) {
    super(props);
    this.fakeUrl = this.fakeUrl.bind(this);
    this.sumit = this.sumit.bind(this);
  }
  componentDidMount() {
    this.props.onClear();
  }
  fakeUrl() {
    if (data.urlApi !== null) {
      return null;
    }
    const { username } = this.props.appreset;
    return (
      <NavLink to={`${URL_RESET_VALIDATE}/${username}/${data.codeValidate}`} activeClassName="active">
        Esta es la url generada para reiniciar contraseña
      </NavLink>
    );
  }
  sumit(e) {
    e.preventDefault();
    this.props.onReset();
  }
  render() {
    const { loading, success, error, username } = this.props.appreset;
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              {success
              ? <span className="clearfix">
                {/* <h1 className="float-left display-3 mr-4">500</h1> */}
                <h1 className="float-left display-3 mr-4">
                  <i className="icon-envelope icons d-block"></i>
                </h1>
                <h4 className="pt-3"><FormattedMessage {...messages.success_title} /></h4>
                <p className="text-muted float-left"><FormattedMessage {...messages.success_paragraph} /></p>
                <br />
                <br />
                <br />
                <br />
                {this.fakeUrl()}
              </span>
              : <Card className="mx-4">
                <CardBlock className="card-body p-4">
                  <LoadingSpiner loading={loading} />
                  <h1><FormattedMessage {...messages.title} /></h1>
                  <p className="text-muted"><FormattedMessage {...messages.paragraph} /></p>
                  <Alert message={error} color="danger" />
                  <InputGroup className="mb-3">
                    <InputGroupAddon>@</InputGroupAddon>
                    <Input
                      type="text"
                      placeholder="Email"
                      value={username}
                      onChange={(evt) => {
                        this.props.onChangeProp('username', evt.target.value);
                      }}
                      onKeyDown={(evt) => { autoFocus.submit(evt, this.props.onReset); }}
                    />
                  </InputGroup>
                  <Button color="success" block onClick={this.sumit}><FormattedMessage {...messages.reset_password} /></Button>
                </CardBlock>
              </Card>
              }
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

AppReset.propTypes = {
  appreset: PropTypes.object,
  onClear: PropTypes.func,
  onChangeProp: PropTypes.func,
  onReset: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  appreset: makeSelectAppReset(),
});

function mapDispatchToProps(dispatch) {
  return {
    onClear: () => dispatch(clearAction()),
    onChangeProp: (prop, value) => dispatch(changePropAction(prop, value)),
    onReset: () => dispatch(attempAction()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'appReset', reducer });
const withSaga = injectSaga({ key: 'appReset', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AppReset);
