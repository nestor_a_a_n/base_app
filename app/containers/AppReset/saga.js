import { takeLatest, call, put, select } from 'redux-saga/effects';
import { ATTEMP } from './constants';
import { successAction, errorAction } from './actions';
import makeSelectAppReset from './selectors';
import request from '../../utils/request';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/AppMainHome/saga.js
  yield takeLatest(ATTEMP, resetPassword);
}

/**
 * Reiniciar contraseña
 */
export function* resetPassword() {
  // Select appReset from store
  const appReset = yield select(makeSelectAppReset());
  const vars = {
    username: appReset.username,
  };
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, 'resetPassword', vars);
    // console.log('result', result)
    if (result.error) {
      yield put(errorAction(result.error.message));
    } else {
      yield put(successAction(result));
    }
  } catch (err) {
    const error = (typeof err === 'string') ? err : 'Error al registrar';
    yield put(errorAction(error));
  }
}
