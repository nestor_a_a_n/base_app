/*
 *
 * AppReset constants
 *
 */

export const DEFAULT_ACTION = 'app/AppReset/DEFAULT_ACTION';

export const CHANGE_PROP = 'app/AppReset/CHANGE_PROP';

export const CLEAR = 'app/AppReset/CLEAR';

export const ATTEMP = 'app/AppReset/ATTEMP';
export const SUCCESS = 'app/AppReset/SUCCESS';
export const ERROR = 'app/AppReset/ERROR';
