/*
 *
 * PageReset reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  CHANGE_PROP,
  CLEAR,
  ATTEMP,
  SUCCESS,
  ERROR,
} from './constants';

export const initialState = fromJS({
  loading: false,
  success: false,
  error: false,
  username: '',
});

function appResetReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case CLEAR:
      return initialState;
    case CHANGE_PROP:
      return state.set(action.prop, action.value);
    case ATTEMP:
      return state
      .set('loading', true)
      .set('success', false)
      .set('error', false);
    case SUCCESS:
      return state
      .set('loading', false)
      .set('success', true);
    case ERROR:
      return state
      .set('loading', false)
      .set('error', action.error);
    default:
      return state;
  }
}

export default appResetReducer;
