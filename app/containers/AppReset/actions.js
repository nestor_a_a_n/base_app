/*
 *
 * AppReset actions
 *
 */

import {
  DEFAULT_ACTION,
  CHANGE_PROP,
  CLEAR,
  ATTEMP,
  SUCCESS,
  ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function changePropAction(prop, value) {
  return {
    type: CHANGE_PROP,
    prop,
    value,
  };
}

export function clearAction() {
  return {
    type: CLEAR,
  };
}

export function attempAction() {
  return {
    type: ATTEMP,
  };
}

export function successAction() {
  return {
    type: SUCCESS,
  };
}

export function errorAction(error) {
  return {
    type: ERROR,
    error,
  };
}
