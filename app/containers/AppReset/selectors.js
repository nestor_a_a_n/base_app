import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the AppReset state domain
 */

const selectAppResetDomain = (state) => state.get('appReset', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by AppReset
 */

const makeSelectAppReset = () =>
  createSelector(selectAppResetDomain, (substate) => substate.toJS());

export default makeSelectAppReset;
export { selectAppResetDomain };
