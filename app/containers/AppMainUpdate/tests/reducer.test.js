
import { fromJS } from 'immutable';
import appMainUpdateReducer from '../reducer';

describe('appMainUpdateReducer', () => {
  it('returns the initial state', () => {
    expect(appMainUpdateReducer(undefined, {})).toEqual(fromJS({}));
  });
});
