/*
 * AppMainUpdate Messages
 *
 * This contains all the text for the AppMainUpdate component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AppMainUpdate.header',
    defaultMessage: 'This is AppMainUpdate container !',
  },
});
