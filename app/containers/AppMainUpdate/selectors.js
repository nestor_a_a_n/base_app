import { createSelector } from 'reselect';

/**
 * Direct selector to the appMainUpdate state domain
 */
const selectAppMainUpdateDomain = (state) => state.get('appMainUpdate');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AppMainUpdate
 */

const makeSelectAppMainUpdate = () => createSelector(
  selectAppMainUpdateDomain,
  (substate) => substate.toJS()
);

export default makeSelectAppMainUpdate;
export {
  selectAppMainUpdateDomain,
};
