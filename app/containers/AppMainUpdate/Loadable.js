/**
 *
 * Asynchronously loads the component for AppMainUpdate
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
