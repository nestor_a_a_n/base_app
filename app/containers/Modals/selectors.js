import { createSelector } from 'reselect';

/**
 * Direct selector to the modals state domain
 */
const selectModalsDomain = (state) => state.get('modals');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Modals
 */

const makeSelectModals = () => createSelector(
  selectModalsDomain,
  (substate) => substate.toJS()
);

export default makeSelectModals;
export {
  selectModalsDomain,
};
