/*
 *
 * Modals actions
 *
 */

import {
  DEFAULT_ACTION,
  MODAL_ADD,
  MODAL_REMOVE,
  MODAL_REMOVE_ALL,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function addAction(cofigModal) {
  return {
    type: MODAL_ADD,
    cofigModal,
  };
}

export function removeAction() {
  return {
    type: MODAL_REMOVE,
  };
}

export function removeAllAction() {
  return {
    type: MODAL_REMOVE_ALL,
  };
}
