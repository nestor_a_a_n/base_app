/*
 *
 * Modals reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  MODAL_ADD,
  MODAL_REMOVE,
  MODAL_REMOVE_ALL,
} from './constants';

const initialState = fromJS({
  configs: [],
});

let stateJs = null;

function modalsReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case MODAL_ADD:
      stateJs = state.toJS();
      stateJs.configs.push(action.cofigModal);
      return state.set('configs', fromJS(stateJs.configs));
    case MODAL_REMOVE:
      stateJs = state.toJS();
      if (stateJs.configs.length > 0) {
        stateJs.configs.splice(-1, 1);
      }
      return state.set('configs', fromJS(stateJs.configs));
    case MODAL_REMOVE_ALL:
      return state.set('configs', initialState);
    default:
      return state;
  }
}

export default modalsReducer;
