
import { fromJS } from 'immutable';
import modalsReducer from '../reducer';

describe('modalsReducer', () => {
  it('returns the initial state', () => {
    expect(modalsReducer(undefined, {})).toEqual(fromJS({}));
  });
});
