/**
 *
 * Modals
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectReducer from 'utils/injectReducer';
import makeSelectModals from './selectors';
import reducer from './reducer';
import { removeAction } from './actions';

export class Modals extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { className, modals, onRemove } = this.props;
    const { configs } = modals;
    let modal = {};
    let open = false;
    if (configs.length > 0) {
      modal = configs[configs.length - 1];
      open = true;
    }
    const { title, body, footerButtoms } = modal;
    return (
      <Modal isOpen={open} toggle={onRemove} className={className}>
        <ModalHeader toggle={onRemove}>{title || ''}</ModalHeader>
        <ModalBody>
          { body || null }
        </ModalBody>
        <ModalFooter>
          { footerButtoms
          ? footerButtoms.map((button, index) => {
            const { color, text, callback } = button;
            const onClickCallback = callback || onRemove;
            const key = `${index}${color}`;
            return (
              <Button key={`modals-button-${key}`} color={color || 'secondary'} onClick={onClickCallback}>{text}</Button>
            );
          })
          : null }
        </ModalFooter>
      </Modal>
    );
  }
}

Modals.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  className: PropTypes.string,
  onRemove: PropTypes.func,
  modals: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  modals: makeSelectModals(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onRemove: () => dispatch(removeAction()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'modals', reducer });

export default compose(
  withReducer,
  withConnect,
)(Modals);
