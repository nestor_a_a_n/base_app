/*
 *
 * Modals constants
 *
 */

export const DEFAULT_ACTION = 'app/Modals/DEFAULT_ACTION';

export const MODAL_ADD = 'app/Modals/MODAL_ADD';
export const MODAL_REMOVE = 'app/Modals/MODAL_REMOVE';
export const MODAL_REMOVE_ALL = 'app/Modals/MODAL_REMOVE_ALL';
