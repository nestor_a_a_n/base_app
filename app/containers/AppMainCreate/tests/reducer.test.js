
import { fromJS } from 'immutable';
import appMainCreateReducer from '../reducer';

describe('appMainCreateReducer', () => {
  it('returns the initial state', () => {
    expect(appMainCreateReducer(undefined, {})).toEqual(fromJS({}));
  });
});
