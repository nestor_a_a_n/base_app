/*
 * AppMainCreate Messages
 *
 * This contains all the text for the AppMainCreate component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AppMainCreate.header',
    defaultMessage: 'This is AppMainCreate container !',
  },
});
