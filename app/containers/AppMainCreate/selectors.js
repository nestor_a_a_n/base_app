import { createSelector } from 'reselect';

/**
 * Direct selector to the appMainCreate state domain
 */
const selectAppMainCreateDomain = (state) => state.get('appMainCreate');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AppMainCreate
 */

const makeSelectAppMainCreate = () => createSelector(
  selectAppMainCreateDomain,
  (substate) => substate.toJS()
);

export default makeSelectAppMainCreate;
export {
  selectAppMainCreateDomain,
};
