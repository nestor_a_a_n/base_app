/**
 *
 * Asynchronously loads the component for AppMainCreate
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
