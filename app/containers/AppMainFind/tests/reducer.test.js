
import { fromJS } from 'immutable';
import appMainFindReducer from '../reducer';

describe('appMainFindReducer', () => {
  it('returns the initial state', () => {
    expect(appMainFindReducer(undefined, {})).toEqual(fromJS({}));
  });
});
