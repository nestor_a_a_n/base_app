import { createSelector } from 'reselect';

/**
 * Direct selector to the appMainFind state domain
 */
const selectAppMainFindDomain = (state) => state.get('appMainFind');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AppMainFind
 */

const makeSelectAppMainFind = () => createSelector(
  selectAppMainFindDomain,
  (substate) => substate.toJS()
);

export default makeSelectAppMainFind;
export {
  selectAppMainFindDomain,
};
