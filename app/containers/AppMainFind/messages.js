/*
 * AppMainFind Messages
 *
 * This contains all the text for the AppMainFind component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AppMainFind.header',
    defaultMessage: 'This is AppMainFind container !',
  },
});
