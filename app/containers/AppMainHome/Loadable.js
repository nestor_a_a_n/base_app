/**
 * Asynchronously loads the component for HomePage
 */
import Loadable from 'react-loadable';

// import LoadingIndicator from 'components/LoadingIndicator';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null, // Si no se encuentra esta linea, no funciona!
  // loading: LoadingIndicator,
});
