import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the mainAppHome state domain
 */

const selectMainAppHomeDomain = state => state.get('mainAppHome', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by AppMainHome
 */

const makeSelectMainAppHome = () =>
  createSelector(selectMainAppHomeDomain, substate => substate.toJS());

export default makeSelectMainAppHome;
export { selectMainAppHomeDomain };
