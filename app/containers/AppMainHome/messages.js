/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  startProjectHeader: {
    id: 'containers.app.main.home.start_project.header',
    defaultMessage: 'Start your next react project in seconds',
  },
  greeting1: {
    id: 'containers.app.main.home.greeting1',
    defaultMessage: 'greeting1',
  },
  greeting2: {
    id: 'containers.app.main.home.greeting2',
    defaultMessage: 'greeting2',
  },
  greeting3: {
    id: 'containers.app.main.home.greeting3',
    defaultMessage: 'greeting3',
  },
  knowMore: {
    id: 'containers.app.main.home.knowMore',
    defaultMessage: 'knowMore',
  },
  tipTitle0: {
    id: 'containers.app.main.home.tipTitle0',
    defaultMessage: 'tipTitle0',
  },
  tipTitle1: {
    id: 'containers.app.main.home.tipTitle1',
    defaultMessage: 'tipTitle1',
  },
  tipTitle2: {
    id: 'containers.app.main.home.tipTitle2',
    defaultMessage: 'tipTitle2',
  },
  tipTitle3: {
    id: 'containers.app.main.home.tipTitle3',
    defaultMessage: 'tipTitle3',
  },
  tipTitle4: {
    id: 'containers.app.main.home.tipTitle4',
    defaultMessage: 'tipTitle4',
  },
  tipTitle5: {
    id: 'containers.app.main.home.tipTitle5',
    defaultMessage: 'tipTitle5',
  },
  tipTitle6: {
    id: 'containers.app.main.home.tipTitle6',
    defaultMessage: 'tipTitle6',
  },
  tipTitle7: {
    id: 'containers.app.main.home.tipTitle7',
    defaultMessage: 'tipTitle7',
  },
  tipTitle8: {
    id: 'containers.app.main.home.tipTitle8',
    defaultMessage: 'tipTitle8',
  },
  tipTitle9: {
    id: 'containers.app.main.home.tipTitle9',
    defaultMessage: 'tipTitle9',
  },
  tipTitle10: {
    id: 'containers.app.main.home.tipTitle10',
    defaultMessage: 'tipTitle10',
  },
  tipTitle11: {
    id: 'containers.app.main.home.tipTitle11',
    defaultMessage: 'tipTitle11',
  },
  tipTitle12: {
    id: 'containers.app.main.home.tipTitle12',
    defaultMessage: 'tipTitle12',
  },
  tipTitle13: {
    id: 'containers.app.main.home.tipTitle13',
    defaultMessage: 'tipTitle13',
  },
  tipTitle14: {
    id: 'containers.app.main.home.tipTitle14',
    defaultMessage: 'tipTitle14',
  },
  tipTitle15: {
    id: 'containers.app.main.home.tipTitle15',
    defaultMessage: 'tipTitle15',
  },
  tipTitle16: {
    id: 'containers.app.main.home.tipTitle16',
    defaultMessage: 'tipTitle16',
  },
  tipTitle17: {
    id: 'containers.app.main.home.tipTitle17',
    defaultMessage: 'tipTitle17',
  },
  tipTitle18: {
    id: 'containers.app.main.home.tipTitle18',
    defaultMessage: 'tipTitle18',
  },
  tipTitle19: {
    id: 'containers.app.main.home.tipTitle19',
    defaultMessage: 'tipTitle19',
  },
  tipTitle20: {
    id: 'containers.app.main.home.tipTitle20',
    defaultMessage: 'tipTitle20',
  },
  tipTitle21: {
    id: 'containers.app.main.home.tipTitle21',
    defaultMessage: 'tipTitle21',
  },
  tipTitle22: {
    id: 'containers.app.main.home.tipTitle22',
    defaultMessage: 'tipTitle22',
  },
  tipTitle23: {
    id: 'containers.app.main.home.tipTitle23',
    defaultMessage: 'tipTitle23',
  },
  tipTitle24: {
    id: 'containers.app.main.home.tipTitle24',
    defaultMessage: 'tipTitle24',
  },
  tipTitle25: {
    id: 'containers.app.main.home.tipTitle25',
    defaultMessage: 'tipTitle25',
  },
  tipTitle26: {
    id: 'containers.app.main.home.tipTitle26',
    defaultMessage: 'tipTitle26',
  },
  tipTitle27: {
    id: 'containers.app.main.home.tipTitle27',
    defaultMessage: 'tipTitle27',
  },
  tipTitle28: {
    id: 'containers.app.main.home.tipTitle28',
    defaultMessage: 'tipTitle28',
  },
  tipTitle29: {
    id: 'containers.app.main.home.tipTitle29',
    defaultMessage: 'tipTitle29',
  },
  tipTitle30: {
    id: 'containers.app.main.home.tipTitle30',
    defaultMessage: 'tipTitle30',
  },
  tipTitle31: {
    id: 'containers.app.main.home.tipTitle31',
    defaultMessage: 'tipTitle31',
  },
  tipTitle32: {
    id: 'containers.app.main.home.tipTitle32',
    defaultMessage: 'tipTitle32',
  },
  tipTitle33: {
    id: 'containers.app.main.home.tipTitle33',
    defaultMessage: 'tipTitle33',
  },
  tipTitle34: {
    id: 'containers.app.main.home.tipTitle34',
    defaultMessage: 'tipTitle34',
  },
  tipTitle35: {
    id: 'containers.app.main.home.tipTitle35',
    defaultMessage: 'tipTitle35',
  },
  useTools: {
    id: 'containers.app.main.home.useTools',
    defaultMessage: 'useTools',
  }
});
