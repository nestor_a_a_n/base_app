/*
 * AppResetValidate Messages
 *
 * This contains all the text for the AppResetValidate component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  title_success: {
    id: 'app.containers.AppResetValidate.title_success',
    defaultMessage: 'Registro completo',
  },
  paragraph_success: {
    id: 'app.containers.AppResetValidate.paragraph_success',
    defaultMessage: 'Se ha completado el registro de usuario, bienvenido.',
  },
  home_success: {
    id: 'app.containers.AppResetValidate.phome_success',
    defaultMessage: 'Ir a inicio',
  },
  title_error: {
    id: 'app.containers.AppResetValidate.title_error',
    defaultMessage: 'Oops, ha ocurrido un error',
  },
  paragraph_error: {
    id: 'app.containers.AppResetValidate.paragraph_error',
    defaultMessage: 'No fue posible autorizar el cambio de contraseña.',
  },
  home_error: {
    id: 'app.containers.AppResetValidate.phome_error',
    defaultMessage: 'Retornar',
  },
  title_success_password: {
    id: 'app.containers.AppResetValidate.title_success_password',
    defaultMessage: 'Contraseña actualizada',
  },
  paragraph_success_password: {
    id: 'app.containers.AppResetValidate.paragraph_success_password',
    defaultMessage: 'Se ha actualizado correctamente la contraseña.',
  },
  title_password: {
    id: 'app.containers.AppResetValidate.title_password',
    defaultMessage: 'Cambio de contraseña',
  },
  paragraph_password: {
    id: 'app.containers.AppResetValidate.paragraph_password',
    defaultMessage: 'Actualiza tu contraseña.',
  },
  update_password: {
    id: 'app.containers.AppResetValidate.update_password',
    defaultMessage: 'Actualizar',
  },
});
