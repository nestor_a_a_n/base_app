import { createSelector } from 'reselect';

/**
 * Direct selector to the appResetValidate state domain
 */
const selectAppResetValidateDomain = (state) => state.get('appResetValidate');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AppResetValidate
 */

const makeSelectAppResetValidate = () => createSelector(
  selectAppResetValidateDomain,
  (substate) => substate.toJS()
);

export default makeSelectAppResetValidate;
export {
  selectAppResetValidateDomain,
};
