
import { fromJS } from 'immutable';
import appResetValidateReducer from '../reducer';

describe('appResetValidateReducer', () => {
  it('returns the initial state', () => {
    expect(appResetValidateReducer(undefined, {})).toEqual(fromJS({}));
  });
});
