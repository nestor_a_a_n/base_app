/*
 *
 * AppResetValidate reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  ATTEMP,
  SUCCESS,
  ERROR,
  CHANGE_PROP,
  ATTEMP_PASSWORD,
  SUCCESS_PASSWORD,
  ERROR_PASSWORD,
} from './constants';

const initialState = fromJS({
  loading: false,
  success: true,
  error: false,
  password: '',
  rePassword: '',
  successPassword: false,
  errorPassword: false,
});

function appResetValidateReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case ATTEMP:
      return state
      .set('loading', true)
      .set('success', false)
      .set('error', false);
    case SUCCESS:
      return state
      .set('loading', false)
      .set('success', true)
      .set('error', false);
    case ERROR:
      return state
      .set('loading', false)
      .set('success', false)
      .set('error', true);
    case CHANGE_PROP:
      return state.set(action.prop, action.value);
    case ATTEMP_PASSWORD:
      return state
      .set('loading', true)
      .set('successPassword', false)
      .set('errorPassword', false);
    case SUCCESS_PASSWORD:
      return state
      .set('loading', false)
      .set('successPassword', true)
      .set('errorPassword', false);
    case ERROR_PASSWORD:
      return state
      .set('loading', false)
      .set('successPassword', false)
      .set('errorPassword', action.error);
    default:
      return state;
  }
}

export default appResetValidateReducer;
