/*
 *
 * AppResetValidate constants
 *
 */

export const DEFAULT_ACTION = 'app/AppResetValidate/DEFAULT_ACTION';

export const ATTEMP = 'app/AppResetValidate/ATTEMP';
export const SUCCESS = 'app/AppResetValidate/SUCCESS';
export const ERROR = 'app/AppResetValidate/ERROR';

export const CHANGE_PROP = 'app/AppResetValidate/CHANGE_PROP';

export const ATTEMP_PASSWORD = 'app/AppResetValidate/ATTEMP_PASSWORD';
export const SUCCESS_PASSWORD = 'app/AppResetValidate/SUCCESS_PASSWORD';
export const ERROR_PASSWORD = 'app/AppResetValidate/ERROR_PASSWORD';
