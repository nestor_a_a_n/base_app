/**
 *
 * Asynchronously loads the component for AppResetValidate
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
