import { takeLatest, select, call, put } from 'redux-saga/effects';
import { ATTEMP, ATTEMP_PASSWORD } from './constants';
import { successAction, errorAction, successPasswordAction, errorPasswordAction } from './actions';
import makeSelectAppResetValidate from './selectors';
import request from '../../utils/request';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(ATTEMP, validateUReset);
  yield takeLatest(ATTEMP_PASSWORD, updatePassword);
}

/**
 * validar el usuario
 */
export function* validateUReset({ username, code }) {
  const vars = {
    username,
    code,
  };
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, 'resetValidate', vars);
    // console.log('login', result)
    if (result.error) {
      yield put(errorAction(result.error.message));
    } else {
      yield put(successAction(result));
      // yield put(loginSuccess(result));
    }
  } catch (err) {
    const error = (typeof err === 'string') ? err : 'Error validacion reset';
    yield put(errorAction(error));
  }
}

/**
 * validar el usuario
 */
export function* updatePassword({ username, code }) {
  const appResetValidate = yield select(makeSelectAppResetValidate());
  const vars = {
    username,
    code,
    password: appResetValidate.password,
    rePassword: appResetValidate.rePassword,
  };
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, 'updatePassword', vars);
    // console.log('login', result)
    if (result.error) {
      yield put(errorPasswordAction(result.error.message));
    } else {
      yield put(successPasswordAction(result));
      // yield put(loginSuccess(result));
    }
  } catch (err) {
    const error = (typeof err === 'string') ? err : 'Error validacion reset';
    yield put(errorPasswordAction(error));
  }
}
