/*
 *
 * AppResetValidate actions
 *
 */

import {
  DEFAULT_ACTION,
  ATTEMP,
  SUCCESS,
  ERROR,
  CHANGE_PROP,
  ATTEMP_PASSWORD,
  SUCCESS_PASSWORD,
  ERROR_PASSWORD,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function attempAction(username, code) {
  return {
    type: ATTEMP,
    username,
    code,
  };
}

export function successAction() {
  return {
    type: SUCCESS,
  };
}

export function errorAction() {
  return {
    type: ERROR,
  };
}

export function changePropAction(prop, value) {
  return {
    type: CHANGE_PROP,
    prop,
    value,
  };
}

export function attempPasswordAction(username, code) {
  return {
    type: ATTEMP_PASSWORD,
    username,
    code,
  };
}

export function successPasswordAction() {
  return {
    type: SUCCESS_PASSWORD,
  };
}

export function errorPasswordAction(error) {
  return {
    type: ERROR_PASSWORD,
    error,
  };
}
