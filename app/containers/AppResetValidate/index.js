/**
 *
 * AppResetValidate
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Container, Row, Col, Button, InputGroup, Input, InputGroupAddon, CardBlock, Card } from 'reactstrap';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

// Actions
import { attempAction, changePropAction, attempPasswordAction } from './actions';
// Selectors
import makeSelectAppResetValidate from './selectors';
// Components
import LoadingSpiner from '../../components/LoadingSpiner';
import Alert from '../../components/Alert';
// Utils
import { URL_BASE } from '../../utils/constants';
import autoFocus from '../../utils/miscellaneous/autoFocus';

export class AppResetValidate extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.renderHtml = this.renderHtml.bind(this);
    this.sumit = this.sumit.bind(this);
    this.inputRePassword = null;
  }
  componentDidMount() {
    const { username, code } = this.props.match.params;
    this.props.onValidate(username, code);
  }
  sumit(e) {
    e.preventDefault();
    const { username, code } = this.props.match.params;
    this.props.onUpdatePassword(username, code);
  }
  renderHtml() {
    const { success, error, password, rePassword, successPassword, errorPassword } = this.props.appresetvalidate;
    if (successPassword) {
      return (
        <Col md="6">
          <span className="clearfix">
            <h1 className="float-left display-3 mr-4">
              <i className="icon-key d-block"></i>
            </h1>
            <h3 className="pt-3"><FormattedMessage {...messages.title_success_password} /></h3>
            <p className="text-muted float-left"><FormattedMessage {...messages.paragraph_success_password} /></p>
          </span>
          <InputGroup className="input-prepend">
            <Button outline color="info" size="sm" block tag={NavLink} to={URL_BASE}>
              <FormattedMessage {...messages.home_success} />
            </Button>
          </InputGroup>
        </Col>
      );
    } else if (success) {
      return (
        <Col md="8">
          <Card className="mx-4">
            <CardBlock className="card-body p-4">
              <h1><FormattedMessage {...messages.title_password} /></h1>
              <p className="text-muted"><FormattedMessage {...messages.paragraph_password} /></p>
              <Alert message={errorPassword} color="danger" />
              <InputGroup className="mb-3">
                <InputGroupAddon><i className="icon-lock"></i></InputGroupAddon>
                <Input
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(evt) => {
                    this.props.onChangeProp('password', evt.target.value);
                  }}
                  onKeyDown={(evt) => { autoFocus.next(evt, this.inputRePassword); }}
                />
              </InputGroup>
              <InputGroup className="mb-3">
                <InputGroupAddon><i className="icon-lock"></i></InputGroupAddon>
                <Input
                  getRef={(el) => { this.inputRePassword = el; }}
                  type="password"
                  placeholder="Repeat password"
                  value={rePassword}
                  onChange={(evt) => {
                    this.props.onChangeProp('rePassword', evt.target.value);
                  }}
                  onKeyDown={(evt) => {
                    autoFocus.submit(evt, this.sumi);
                  }}
                />
              </InputGroup>
              <Button color="success" block onClick={this.sumit}><FormattedMessage {...messages.update_password} /></Button>
            </CardBlock>
          </Card>
        </Col>
      );
    } else if (error) {
      return (
        <Col md="6">
          <span className="clearfix">
            <h1 className="float-left display-3 mr-4">
              <i className="icon-ban d-block"></i>
            </h1>
            <h3 className="pt-3"><FormattedMessage {...messages.title_error} /></h3>
            <p className="text-muted float-left"><FormattedMessage {...messages.paragraph_error} /></p>
          </span>
          <InputGroup className="input-prepend">
            <Button outline color="info" size="sm" block tag={NavLink} to={URL_BASE}>
              <FormattedMessage {...messages.home_error} />
            </Button>
          </InputGroup>
        </Col>
      );
    }
    return null;
  }
  render() {
    // http://localhost:3000/reset-validate/demo/code
    const { loading } = this.props.appresetvalidate;
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <LoadingSpiner loading={loading} />
            {this.renderHtml()}
          </Row>
        </Container>
      </div>
    );
  }
}

AppResetValidate.propTypes = {
  onValidate: PropTypes.func,
  appresetvalidate: PropTypes.object,
  match: PropTypes.object,
  onChangeProp: PropTypes.func,
  onUpdatePassword: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  appresetvalidate: makeSelectAppResetValidate(),
});

function mapDispatchToProps(dispatch) {
  return {
    // dispatch,
    onValidate: (username, code) => dispatch(attempAction(username, code)),
    onChangeProp: (prop, value) => dispatch(changePropAction(prop, value)),
    onUpdatePassword: (username, code) => dispatch(attempPasswordAction(username, code)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'appResetValidate', reducer });
const withSaga = injectSaga({ key: 'appResetValidate', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AppResetValidate);
