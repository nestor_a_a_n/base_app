/*
 *
 * AppLogin constants
 *
 */

export const DEFAULT_ACTION = 'app/AppLogin/DEFAULT_ACTION';

export const CHANGE_PROP = 'app/MainApp/CHANGE_PROP';

export const CLEAR = 'app/MainApp/CLEAR';

export const ATTEMPT = 'app/MainApp/ATTEMPT';
export const SUCCESS = 'app/MainApp/SUCCESS';
export const ERROR = 'app/MainApp/ERROR';
