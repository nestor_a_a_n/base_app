import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the appLogin state domain
 */

const selectAppLoginDomain = state => state.get('appLogin', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by AppLogin
 */

const makeSelectAppLogin = () =>
  createSelector(selectAppLoginDomain, substate => substate.toJS());

export default makeSelectAppLogin;
export { selectAppLoginDomain };
