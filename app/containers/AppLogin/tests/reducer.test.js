import { fromJS } from 'immutable';
import pageLoginReducer from '../reducer';

describe('pageLoginReducer', () => {
  it('returns the initial state', () => {
    expect(pageLoginReducer(undefined, {})).toEqual(fromJS({}));
  });
});
