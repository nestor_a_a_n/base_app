/*
 * PageLogin Messages
 *
 * This contains all the text for the PageLogin component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  login: {
    id: 'containers.app.login.login',
    defaultMessage: 'Login',
  },
  sign_in: {
    id: 'containers.app.login.sign_in',
    defaultMessage: 'Sign In to your account',
  },
  forgot_password: {
    id: 'containers.app.login.forgot_password',
    defaultMessage: 'Forgot password?',
  },
  sign_up: {
    id: 'containers.app.login.sign_up',
    defaultMessage: 'Sign up',
  },
  sign_up_paragraph: {
    id: 'containers.app.login.sign_up_paragraph',
    defaultMessage: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  },
  register_now: {
    id: 'containers.app.login.register_now',
    defaultMessage: 'Register Now!',
  },
  password: {
    id: 'containers.app.login.password',
    defaultMessage: 'Password',
  },
  username: {
    id: 'containers.app.login.username',
    defaultMessage: 'Username',
  }
});
