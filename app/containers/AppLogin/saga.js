import { call, put, select, takeLatest } from 'redux-saga/effects';
import { ATTEMPT } from './constants';
import { login, logout } from '../App/actions';
import { clearAction, errorAction } from './actions';
import makeSelectPageLogin from './selectors';

// import request from '../../utils/request';
import configAdmin from '../../configAdmin';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/AppMainHome/saga.js
  yield takeLatest(ATTEMPT, loginUser);
}

/**
 * Autenticar el usuario
 */
export function* loginUser() {
  // Select pagelogin from store
  const pagelogin = yield select(makeSelectPageLogin());
  const { restClient } = configAdmin;
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(restClient.login, pagelogin);
    if (result.error) {
      yield put(logout());
      yield put(errorAction(result.error.message));
    } else {
      yield put(clearAction());
      restClient.setAuth(result.data);
      yield put(login(result));
    }
  } catch (err) {
    const error = (typeof err === 'string') ? err : 'Error en la petición login';
    yield put(logout());
    yield put(errorAction(error));
  }
}
