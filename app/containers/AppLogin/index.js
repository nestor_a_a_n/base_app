/**
 *
 * AppLogin
 *
 */

import React from 'react';
import { Container, Row, Col, CardGroup, Card, CardBlock, Button, Input, InputGroup, InputGroupAddon } from 'reactstrap';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';
import reducer from './reducer';
import saga from './saga';

// Actions
import { changePropAction, attemptAction } from './actions';
// Selectors
import makeSelectAppLogin from './selectors';
// Mesages
import messages from './messages';
// Images
// import ImageChronosLogo from '../../images/chronos-app-logo.png';
// Custom
import LoadingSpiner from '../../components/LoadingSpiner';
import Alert from '../../components/Alert';

// Utils
import autoFocus from '../../utils/miscellaneous/autoFocus';
import { URL_REGISTER, URL_RESET } from '../../utils/constants';

/* eslint-disable react/prefer-stateless-function */
export class AppLogin extends React.PureComponent {
  constructor(props) {
    super(props);
    this.inputPassword = null;
    this.sumit = this.sumit.bind(this);
  }
  sumit(e) {
    e.preventDefault();
    this.props.onLoginAttemp();
  }
  render() {
    const { username, password, loading, error } = this.props.appLogin;
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="mb-0">
                <Card className="p-4">
                  <LoadingSpiner loading={loading} />
                  <CardBlock className="card-body" id="cardBlock">
                    <h1><FormattedMessage {...messages.login} /></h1>
                    <p className="text-muted"><FormattedMessage {...messages.sign_in} /></p>
                    <Alert message={error} color="danger" />
                    <InputGroup className="mb-3">
                      <InputGroupAddon>
                        <i className="icon-user"></i>
                      </InputGroupAddon>
                      <Input
                        type="text"
                        placeholder="Username"
                        value={username}
                        onChange={(evt) => {
                          this.props.onChangeProp('username', evt.target.value);
                        }}
                        onKeyDown={(evt) => { autoFocus.next(evt, this.inputPassword); }}
                      />
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon>
                        <i className="icon-lock"></i>
                      </InputGroupAddon>
                      <Input
                        getRef={(el) => { this.inputPassword = el; }}
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={(evt) => {
                          this.props.onChangeProp('password', evt.target.value);
                        }}
                        onKeyDown={(evt) => { autoFocus.submit(evt, this.props.onLoginAttemp); }}
                      />
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" className="px-4" onClick={this.sumit}><FormattedMessage {...messages.login} /></Button>
                      </Col>
                      <Col xs="6" className="text-right">
                        <Button color="link" className="px-0" onClick={() => { this.props.history.push(URL_RESET); }}><FormattedMessage {...messages.forgot_password} /></Button>
                      </Col>
                    </Row>
                  </CardBlock>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CardBlock className="card-body text-center">
                    <div>
                      <h2><FormattedMessage {...messages.sign_up} /></h2>
                      <p><FormattedMessage {...messages.sign_up_paragraph} /></p>
                      <Button color="primary" className="mt-3" active onClick={() => { this.props.history.push(URL_REGISTER); }}><FormattedMessage {...messages.register_now} /></Button>
                    </div>
                  </CardBlock>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

AppLogin.propTypes = {
  history: PropTypes.object,
  appLogin: PropTypes.object,
  onLoginAttemp: PropTypes.func.isRequired,
  onChangeProp: PropTypes.func.isRequired,
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  appLogin: makeSelectAppLogin(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onChangeProp: (prop, value) => dispatch(changePropAction(prop, value)),
    onLoginAttemp: () => dispatch(attemptAction()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'appLogin', reducer });
const withSaga = injectSaga({ key: 'appLogin', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AppLogin);
