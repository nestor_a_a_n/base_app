/*
 *
 * PageLogin reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  CHANGE_PROP,
  CLEAR,
  ATTEMPT,
  SUCCESS,
  ERROR,
} from './constants';

export const initialState = fromJS({
  username: '',
  password: '',
  loading: false,
  error: false,
});

function pageLoginReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case CHANGE_PROP:
      return state.set(action.prop, action.value);
    case CLEAR:
      return initialState;
    case ATTEMPT:
      return state
        .set('loading', true)
        .set('error', false);
    case SUCCESS:
      return state
        .set('loading', false);
    case ERROR:
      return state
        .set('loading', false)
        .set('error', action.error);
    default:
      return state;
  }
}

export default pageLoginReducer;
