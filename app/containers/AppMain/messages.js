/*
 * MainApp Messages
 *
 * This contains all the text for the MainApp component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'containers.app.main.header',
    defaultMessage: 'This is MainApp container !',
  },
});
