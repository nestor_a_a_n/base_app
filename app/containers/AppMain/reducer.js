/*
 *
 * AppMain reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION
} from './constants';
import nav from './_nav'

export const initialState = fromJS({
  nav
});

function mainAppReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    default:
      return state;
  }
}

export default mainAppReducer;
