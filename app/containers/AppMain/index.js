/**
 *
 * AppMain
 *
 */

import React from 'react';
import {
  Container,
} from 'reactstrap';
import { Helmet } from 'react-helmet';
// import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Switch, Redirect } from 'react-router-dom';

import { connect } from 'react-redux';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
// Actions
import { logout } from '../App/actions';
// Selectors
import makeSelectMainApp from './selectors';
import { makeSelectDataUser } from '../App/selectors';
// Reducers
// import messages from './messages';
// Components
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import PrivateRoute from '../../components/PrivateRoute';
import Sidebar from '../../components/Sidebar';
import Aside from '../../components/Aside';
// Pages
import AppMainHome from '../AppMainHome';
import AppMainList from '../AppMainList';

import Modals from '../Modals';

// Configuracion
import configAdmin from '../../configAdmin';

// Utils
import nav from './_nav';

/* eslint-disable react/prefer-stateless-function */
export class MainApp extends React.PureComponent {
  render() {
    const { dataUser } = this.props;
    let { resources } = configAdmin;
    if (typeof resources === 'undefined') {
      resources = [];
    }
    // Agregar al menu de navegacion los recursos
    const newNav = JSON.parse(JSON.stringify(nav));
    for (let i = 0; i < resources.length; i += 1) {
      const { name, icon } = resources[i];
      newNav.items.push({ name, url: `/${name}`, icon: icon || 'icon-star' });
    }
    newNav.items.push({
      name: 'Template',
      url: '/template',
      icon: 'icon-layers',
      variant: 'danger',
    });
    return (
      <div className="app">
        <Helmet
          titleTemplate="%s - React.js Boilerplate"
          defaultTitle="React.js Boilerplate"
        >
          <meta
            name="description"
            content="A React.js Boilerplate application"
          />
        </Helmet>
        <Header {...this.props} />
        <div className="app-body">
          <Sidebar nav={newNav} />
          <main className="main">
            <Container fluid>
              <Switch>
                <PrivateRoute path="/home" component={AppMainHome} something={dataUser} />
                {resources.map((resource) => {
                  const { name, list, create, update, remove } = resource;
                  if (typeof list === 'object') {
                    return (<PrivateRoute key={`${name}-list`} path={`/${name}`} config={list} source={name} component={AppMainList} create={create} update={update} remove={remove} />);
                  }
                  return null;
                })}
                <Redirect exact from="/" to="/home" />
              </Switch>
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
        <Modals />
      </div>
    );
  }
}

MainApp.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  dataUser: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  mainapp: makeSelectMainApp(),
  dataUser: makeSelectDataUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onLogout: () => dispatch(logout()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'mainApp', reducer });
const withSaga = injectSaga({ key: 'mainApp', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(MainApp);
