/**
 *
 * Asynchronously loads the component for AppMain
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
