import { createSelector } from 'reselect';

/**
 * Direct selector to the appMainDelete state domain
 */
const selectAppMainDeleteDomain = (state) => state.get('appMainDelete');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AppMainDelete
 */

const makeSelectAppMainDelete = () => createSelector(
  selectAppMainDeleteDomain,
  (substate) => substate.toJS()
);

export default makeSelectAppMainDelete;
export {
  selectAppMainDeleteDomain,
};
