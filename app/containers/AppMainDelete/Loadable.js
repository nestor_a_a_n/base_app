/**
 *
 * Asynchronously loads the component for AppMainDelete
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
