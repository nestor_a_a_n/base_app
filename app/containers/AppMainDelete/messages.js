/*
 * AppMainDelete Messages
 *
 * This contains all the text for the AppMainDelete component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AppMainDelete.header',
    defaultMessage: 'This is AppMainDelete container !',
  },
});
