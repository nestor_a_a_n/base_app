
import { fromJS } from 'immutable';
import appMainDeleteReducer from '../reducer';

describe('appMainDeleteReducer', () => {
  it('returns the initial state', () => {
    expect(appMainDeleteReducer(undefined, {})).toEqual(fromJS({}));
  });
});
