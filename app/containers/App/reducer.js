/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  LOGIN,
  LOGOUT,
} from './constants';

import store from '../../utils/miscellaneous/store';
import configAdmin from '../../configAdmin';
import { DATA_USER } from '../../utils/constants';

// The initial state of the App
const initialState = fromJS({
  payload: store.get(DATA_USER, {}),
});
configAdmin.restClient.setAuth(store.get(DATA_USER, {}));

function appReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      store.set(DATA_USER, action.payload.data);
      return state
        .set('payload', fromJS(action.payload.data));
    case LOGOUT:
      store.set(DATA_USER, {});
      return state
        .set('payload', fromJS({}));
    default:
      return state;
  }
}

export default appReducer;
