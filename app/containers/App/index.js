/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
// import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

// Pages
import AppMain from 'containers/AppMain/Loadable';
import AppLogin from 'containers/AppLogin/Loadable';
import App404 from 'containers/App404/Loadable';
import App500 from 'containers/App500/Loadable';
import AppReset from 'containers/AppReset/Loadable';
import AppResetValidate from 'containers/AppResetValidate/Loadable';
import AppRegister from 'containers/AppRegister/Loadable';
import AppRegisterValidate from 'containers/AppRegisterValidate/Loadable';
import Template from '../../admin_template/App';
import TemplateLogin from '../../admin_template/Pages/Login';
import TemplatePage404 from '../../admin_template/Pages/Page404';
import TemplatePage500 from '../../admin_template/Pages/Page500';
import TemplateRegister from '../../admin_template/Pages/Register';

// Components
import PrivateRoute from '../../components/PrivateRoute';

import { URL_LOGIN, URL_RESET, URL_RESET_VALIDATE, URL_REGISTER, URL_REGISTER_VALIDATE, URL_404, URL_500, URL_TEMPLATE_LOGIN, URL_TEMPLATE_404, URL_TEMPLATE_500, URL_TEMPLATE_REGISTER, URL_TEMPLATE, URL_BASE } from '../../utils/constants';

export default function App() {
  return (
    <Switch>
      {/* {configAdmin.auth
      ? <PrivateRoute inverse path={URL_LOGIN} name="Page Login" component={AppLogin} />
      :null} */}
      <PrivateRoute inverse path={URL_LOGIN} name="Page Login" component={AppLogin} />
      <Route path={`${URL_RESET_VALIDATE}/:username/:code`} name="Page Validate Reset" component={AppResetValidate} />
      <Route path={URL_RESET} name="Page Reset" component={AppReset} />
      <Route path={`${URL_REGISTER_VALIDATE}/:email/:code`} name="Page Validate Register" component={AppRegisterValidate} />
      <Route path={URL_REGISTER} name="Page Register" component={AppRegister} />
      <Route exact path={URL_404} name="Page 404" component={App404} />
      <Route exact path={URL_500} name="Page 500" component={App500} />
      <Route path={URL_TEMPLATE_LOGIN} component={TemplateLogin} />
      <Route path={URL_TEMPLATE_404} component={TemplatePage404} />
      <Route path={URL_TEMPLATE_500} component={TemplatePage500} />
      <Route path={URL_TEMPLATE_REGISTER} component={TemplateRegister} />
      <Route path={URL_TEMPLATE} component={Template} />
      <Route path={URL_BASE} name="Main" component={AppMain} />
      <Route path="" component={App404} />
    </Switch>
  );
}

// https://github.com/redound/phalcon-rest-boilerplate

// Nomenclatura para la creacion de paginas
// {NameContainer}{Name}
// AppLogin
// AppMainHome
// AppAdminHome

// https://github.com/redound/phalcon-rest/issues/33
// https://github.com/redound/phalcon-rest-boilerplate/issues/46
// https://github.com/redound/phalcon-rest/blob/4820d8623930e1ef6ca6004aff5b0facfbf561fe/src/PhalconRest/Middleware/AuthorizationMiddleware.php

// https://marmelab.com/admin-on-rest-demo/#/login
