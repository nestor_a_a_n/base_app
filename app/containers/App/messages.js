import { defineMessages } from 'react-intl';

export default defineMessages({
  HEAD: {
    id: 'app.HEAD',
    defaultMessage: 'Head',
  },
});
