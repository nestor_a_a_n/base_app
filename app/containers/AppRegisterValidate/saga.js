import { takeLatest, call, put } from 'redux-saga/effects';
import { ATTEMP } from './constants';
import { successAction, errorAction } from './actions';
// import { loginSuccess } from '../App/actions';
import request from '../../utils/request';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(ATTEMP, validateUser);
}

/**
 * validar el usuario
 */
export function* validateUser({ email, code }) {
  const vars = {
    username: email,
    code,
  };
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, 'usersValidate', vars);
    // console.log('login', result)
    if (result.error) {
      yield put(errorAction(result.error.message));
    } else {
      yield put(successAction(result));
      // yield put(loginSuccess(result));
    }
  } catch (err) {
    const error = (typeof err === 'string') ? err : 'Error en la petición login';
    yield put(errorAction(error));
  }
}
