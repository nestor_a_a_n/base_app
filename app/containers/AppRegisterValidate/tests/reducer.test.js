
import { fromJS } from 'immutable';
import appRegisterValidateReducer from '../reducer';

describe('appRegisterValidateReducer', () => {
  it('returns the initial state', () => {
    expect(appRegisterValidateReducer(undefined, {})).toEqual(fromJS({}));
  });
});
