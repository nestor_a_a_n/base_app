/*
 *
 * AppRegisterValidate reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  ATTEMP,
  SUCCESS,
  ERROR,
} from './constants';

const initialState = fromJS({
  loading: false,
  success: false,
  error: false,
});

function appRegisterValidateReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case ATTEMP:
      return state
      .set('loading', true)
      .set('success', false)
      .set('error', false);
    case SUCCESS:
      return state
      .set('loading', false)
      .set('success', true)
      .set('error', false);
    case ERROR:
      return state
      .set('loading', false)
      .set('success', false)
      .set('error', true);
    default:
      return state;
  }
}

export default appRegisterValidateReducer;
