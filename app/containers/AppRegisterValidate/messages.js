/*
 * AppRegisterValidate Messages
 *
 * This contains all the text for the AppRegisterValidate component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  title_success: {
    id: 'app.containers.AppRegisterValidate.title_success',
    defaultMessage: 'Registro completo',
  },
  paragraph_success: {
    id: 'app.containers.AppRegisterValidate.paragraph_success',
    defaultMessage: 'Se ha completado el registro de usuario, bienvenido.',
  },
  home_success: {
    id: 'app.containers.AppRegisterValidate.phome_success',
    defaultMessage: 'Ir a inicio',
  },
  title_error: {
    id: 'app.containers.AppRegisterValidate.title_error',
    defaultMessage: 'Oops, ha ocurrido un error',
  },
  paragraph_error: {
    id: 'app.containers.AppRegisterValidate.paragraph_error',
    defaultMessage: 'No fue posible completar el registro.',
  },
  home_error: {
    id: 'app.containers.AppRegisterValidate.phome_error',
    defaultMessage: 'Ir a inicio de sesion',
  },
});
