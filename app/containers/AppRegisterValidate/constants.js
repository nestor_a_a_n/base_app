/*
 *
 * AppRegisterValidate constants
 *
 */

export const DEFAULT_ACTION = 'app/AppRegisterValidate/DEFAULT_ACTION';

export const ATTEMP = 'app/AppRegisterValidate/ATTEMP';
export const SUCCESS = 'app/AppRegisterValidate/SUCCESS';
export const ERROR = 'app/AppRegisterValidate/ERROR';
