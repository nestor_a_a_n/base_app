/**
 *
 * AppRegisterValidate
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Container, Row, Col, Button, InputGroup } from 'reactstrap';

import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

// Actions
import { attempAction } from './actions';
// Selectors
import makeSelectAppRegisterValidate from './selectors';
// Components
import LoadingSpiner from '../../components/LoadingSpiner';
// Utils
import { URL_BASE } from '../../utils/constants';

export class AppRegisterValidate extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { email, code } = this.props.match.params;
    this.props.onValidate(email, code);
  }
  render() {
    // http://localhost:3000/register-validate/algo@algo.com/code
    const { loading, success, error } = this.props.appregistervalidate;
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <LoadingSpiner loading={loading} />
            {success
            ? <Col md="6">
              <span className="clearfix">
                <h1 className="float-left display-3 mr-4">
                  <i className="icon-envelope-open d-block"></i>
                </h1>
                <h3 className="pt-3"><FormattedMessage {...messages.title_success} /></h3>
                <p className="text-muted float-left"><FormattedMessage {...messages.paragraph_success} /></p>
              </span>
              <InputGroup className="input-prepend">
                <Button outline color="info" size="sm" block tag={NavLink} to={URL_BASE}>
                  <FormattedMessage {...messages.home_success} />
                </Button>
              </InputGroup>
            </Col>
            : null }
            {error
            ? <Col md="6">
              <span className="clearfix">
                <h1 className="float-left display-3 mr-4">
                  <i className="icon-ban d-block"></i>
                </h1>
                <h3 className="pt-3"><FormattedMessage {...messages.title_error} /></h3>
                <p className="text-muted float-left"><FormattedMessage {...messages.paragraph_error} /></p>
              </span>
              <InputGroup className="input-prepend">
                <Button outline color="info" size="sm" block tag={NavLink} to={URL_BASE}>
                  <FormattedMessage {...messages.home_error} />
                </Button>
              </InputGroup>
            </Col>
            : null}
          </Row>
        </Container>
      </div>
    );
  }
}

AppRegisterValidate.propTypes = {
  appregistervalidate: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  onValidate: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  appregistervalidate: makeSelectAppRegisterValidate(),
});

function mapDispatchToProps(dispatch) {
  return {
    // dispatch,
    onValidate: (email, code) => dispatch(attempAction(email, code)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'appRegisterValidate', reducer });
const withSaga = injectSaga({ key: 'appRegisterValidate', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AppRegisterValidate);
