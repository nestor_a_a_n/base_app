import { createSelector } from 'reselect';

/**
 * Direct selector to the appRegisterValidate state domain
 */
const selectAppRegisterValidateDomain = (state) => state.get('appRegisterValidate');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AppRegisterValidate
 */

const makeSelectAppRegisterValidate = () => createSelector(
  selectAppRegisterValidateDomain,
  (substate) => substate.toJS()
);

export default makeSelectAppRegisterValidate;
export {
  selectAppRegisterValidateDomain,
};
