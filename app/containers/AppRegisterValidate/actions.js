/*
 *
 * AppRegisterValidate actions
 *
 */

import {
  DEFAULT_ACTION,
  ATTEMP,
  SUCCESS,
  ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function attempAction(email, code) {
  return {
    type: ATTEMP,
    email,
    code,
  };
}

export function successAction() {
  return {
    type: SUCCESS,
  };
}

export function errorAction() {
  return {
    type: ERROR,
  };
}
