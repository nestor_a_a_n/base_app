/*
 *
 * AppRegister actions
 *
 */

import {
  DEFAULT_ACTION,
  CHANGE_PROP,
  ATTEMP,
  SUCCESS,
  ERROR,
  CLEAR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function changePropAction(prop, value) {
  return {
    type: CHANGE_PROP,
    prop,
    value,
  };
}

export function attempAction() {
  return {
    type: ATTEMP,
  };
}

export function successAction() {
  return {
    type: SUCCESS,
  };
}

export function errorAction(error) {
  return {
    type: ERROR,
    error,
  };
}

export function clearAction() {
  return {
    type: CLEAR,
  };
}
