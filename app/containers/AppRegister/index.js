/**
 *
 * AppRegister
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { Container, Row, Col, Card, CardBlock, Button, Input, InputGroup, InputGroupAddon } from 'reactstrap';

import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';
import makeSelectAppRegister from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

// Actions
import { changePropAction, attempAction, clearAction } from './actions';

// Components
import LoadingSpiner from '../../components/LoadingSpiner';
import Feedback, { feedbackClass } from '../../components/Feedback';

// Utils
import autoFocus from '../../utils/miscellaneous/autoFocus';
import data from '../../utils/miscellaneous/data';
import { URL_REGISTER_VALIDATE } from '../../utils/constants';

/* eslint-disable react/prefer-stateless-function */
export class AppRegister extends React.PureComponent {
  constructor(props) {
    super(props);
    this.inputPassword = null;
    this.inputRePassword = null;
    this.fakeUrl = this.fakeUrl.bind(this);
    this.sumit = this.sumit.bind(this);
  }
  componentDidMount() {
    this.props.onClear();
  }
  fakeUrl() {
    if (data.urlApi !== null) {
      return null;
    }
    const { email } = this.props.appregister;
    return (
      <NavLink to={`${URL_REGISTER_VALIDATE}/${email}/${data.codeValidate}`} activeClassName="active">
        Esta es la url generada para activar la cuenta
      </NavLink>
    );
  }
  sumit(e) {
    e.preventDefault();
    this.props.onRegister();
  }
  render() {
    const { loading, success, error, email, passowrd, rePassword } = this.props.appregister;
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              {success
              ? <span className="clearfix">
                {/* <h1 className="float-left display-3 mr-4">500</h1> */}
                <h1 className="float-left display-3 mr-4">
                  <i className="icon-envelope icons d-block"></i>
                </h1>
                <h4 className="pt-3"><FormattedMessage {...messages.success_title} /></h4>
                <p className="text-muted float-left"><FormattedMessage {...messages.success_paragraph} /></p>
                <br />
                <br />
                <br />
                <br />
                {this.fakeUrl()}
              </span>
              : <Card className="mx-4">
                <CardBlock className="card-body p-4">
                  <LoadingSpiner loading={loading} />
                  <h1><FormattedMessage {...messages.title} /></h1>
                  <p className="text-muted"><FormattedMessage {...messages.paragraph} /></p>
                  <InputGroup className="mb-3">
                    <InputGroupAddon >@</InputGroupAddon>
                    <Input
                      type="text"
                      placeholder="Email"
                      value={email}
                      onChange={(evt) => {
                        this.props.onChangeProp('email', evt.target.value);
                      }}
                      onKeyDown={(evt) => { autoFocus.next(evt, this.inputPassword); }}
                      className={feedbackClass(error, 'username', false)}
                    />
                    <Feedback message={error} prop="username" valid={false} />
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon><i className="icon-lock"></i></InputGroupAddon>
                    <Input
                      getRef={(el) => { this.inputPassword = el; }}
                      type="password"
                      placeholder="Password"
                      value={passowrd}
                      onChange={(evt) => {
                        this.props.onChangeProp('password', evt.target.value);
                      }}
                      onKeyDown={(evt) => { autoFocus.next(evt, this.inputRePassword); }}
                      className={feedbackClass(error, 'password', false)}
                    />
                    <Feedback message={error} prop="password" valid={false} />
                  </InputGroup>
                  <InputGroup className="mb-4">
                    <InputGroupAddon><i className="icon-lock"></i></InputGroupAddon>
                    <Input
                      getRef={(el) => { this.inputRePassword = el; }}
                      type="password"
                      placeholder="Repeat password"
                      value={rePassword}
                      onChange={(evt) => {
                        this.props.onChangeProp('rePassword', evt.target.value);
                      }}
                      onKeyDown={(evt) => { autoFocus.submit(evt, this.props.onRegister); }}
                      className={feedbackClass(error, 'rePassword', false)}
                    />
                    <Feedback message={error} prop="rePassword" valid={false} />
                  </InputGroup>
                  <Button color="success" block onClick={this.sumit}><FormattedMessage {...messages.create_account} /></Button>
                </CardBlock>
              </Card>
              }
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

AppRegister.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  appregister: PropTypes.object,
  onChangeProp: PropTypes.func,
  onRegister: PropTypes.func,
  onClear: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  appregister: makeSelectAppRegister(),
});

function mapDispatchToProps(dispatch) {
  return {
    // dispatch,
    onChangeProp: (prop, value) => dispatch(changePropAction(prop, value)),
    onRegister: () => dispatch(attempAction()),
    onClear: () => dispatch(clearAction()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'appRegister', reducer });
const withSaga = injectSaga({ key: 'appRegister', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AppRegister);
