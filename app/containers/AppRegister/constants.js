/*
 *
 * AppRegister constants
 *
 */

export const DEFAULT_ACTION = 'app/AppRegister/DEFAULT_ACTION';

export const CHANGE_PROP = 'app/AppRegister/CHANGE_PROP';
export const ATTEMP = 'app/AppRegister/ATTEMP';
export const SUCCESS = 'app/AppRegister/SUCCESS';
export const ERROR = 'app/AppRegister/ERROR';

export const CLEAR = 'app/AppRegister/CLEAR';
