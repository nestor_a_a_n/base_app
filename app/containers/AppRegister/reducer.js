/*
 *
 * AppRegister reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  CHANGE_PROP,
  ATTEMP,
  SUCCESS,
  ERROR,
  CLEAR,
} from './constants';

export const initialState = fromJS({
  loading: false,
  success: false,
  error: false,
  email: '',
  password: '',
  rePassword: '',
});

function appRegisterReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case CHANGE_PROP:
      return state.set(action.prop, action.value);
    case ATTEMP:
      return state
      .set('loading', true)
      .set('success', false)
      .set('error', false);
    case SUCCESS:
      return state
      .set('loading', false)
      .set('success', true)
      .set('error', false);
    case ERROR:
      return state
      .set('loading', false)
      .set('success', false)
      .set('error', action.error);
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export default appRegisterReducer;
