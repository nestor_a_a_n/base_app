import { takeLatest, call, put, select } from 'redux-saga/effects';
import { ATTEMP } from './constants';
import { successAction, errorAction } from './actions';
import makeSelectAppRegister from './selectors';
import request from '../../utils/request';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/AppMainHome/saga.js
  yield takeLatest(ATTEMP, registerUser);
}

/**
 * registrar el usuario
 */
export function* registerUser() {
  // Select appRegister from store
  const appRegister = yield select(makeSelectAppRegister());
  const vars = {
    username: appRegister.email,
    password: appRegister.password,
    rePassword: appRegister.rePassword,
  };
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, 'usersRegister', vars);
    // console.log('result', result)
    if (result.error) {
      yield put(errorAction(result.error.message));
    } else {
      yield put(successAction(result));
    }
  } catch (err) {
    const error = (typeof err === 'string') ? err : 'Error al registrar';
    yield put(errorAction(error));
  }
}
