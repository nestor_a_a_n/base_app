import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the appRegister state domain
 */

const selectAppRegisterDomain = state => state.get('appRegister', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by AppRegister
 */

const makeSelectAppRegister = () =>
  createSelector(selectAppRegisterDomain, substate => substate.toJS());

export default makeSelectAppRegister;
export { selectAppRegisterDomain };
