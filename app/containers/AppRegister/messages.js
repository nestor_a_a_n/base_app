/*
 * AppRegister Messages
 *
 * This contains all the text for the AppRegister component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'app.containers.AppRegister.title',
    defaultMessage: 'Register',
  },
  paragraph: {
    id: 'app.containers.AppRegister.paragraph',
    defaultMessage: 'Create your account',
  },
  create_account: {
    id: 'app.containers.AppRegister.create_account',
    defaultMessage: 'Create Account',
  },
  success_title: {
    id: 'app.containers.AppRegister.success_title',
    defaultMessage: 'Un paso mas',
  },
  success_paragraph: {
    id: 'app.containers.AppRegister.success_paragraph',
    defaultMessage: 'Revisa tu correo y sigue las instrucciones para completar el registro.',
  },
});
