import { fromJS } from 'immutable';
import appRegisterReducer from '../reducer';

describe('appRegisterReducer', () => {
  it('returns the initial state', () => {
    expect(appRegisterReducer(undefined, {})).toEqual(fromJS({}));
  });
});
