/**
 *
 * Asynchronously loads the component for AppRegister
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
