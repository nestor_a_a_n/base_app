import { takeLatest, call, put } from 'redux-saga/effects';
import { ATTEMP, DELETE_ATTEMP } from './constants';
import { successAction, errorAction, deleteSuccessAction, deleteErrorAction } from './actions';
// import makeSelectAppMainUsers from './selectors';
// import request from '../../utils/request';
import configAdmin from '../../configAdmin';


// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(ATTEMP, getList);
  yield takeLatest(DELETE_ATTEMP, getList);
}

/**
 * Obtener los usuarios
 */
export function* getList({ query, source }) {
  // Select appReset from store
  // const appReset = yield select(makeSelectAppMainUsers());
  const vars = { query, source };
  const { restClient } = configAdmin;
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(restClient.list, vars);
    // console.log('result', result);
    if (result.error) {
      yield put(errorAction(result.error.message));
    } else {
      yield put(successAction(result[source], result.paginate || null));
    }
  } catch (err) {
    const error = (typeof err === 'string') ? err : `Error al obtener ${source}`;
    yield put(errorAction(error));
  }
}

/**
 * Delete item
 */
export function* deleteItem({ id, source }) {
  const { restClient } = configAdmin;
  const vars = { id, source };
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(restClient.delete, vars);
    // console.log('result', result);
    if (result.error) {
      yield put(deleteErrorAction(result.error.message));
    } else {
      yield put(deleteSuccessAction(result));
    }
  } catch (err) {
    const error = (typeof err === 'string') ? err : `Error al obtener ${source}`;
    yield put(deleteErrorAction(error));
  }
}
