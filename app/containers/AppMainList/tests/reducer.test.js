
import { fromJS } from 'immutable';
import appMainUsersReducer from '../reducer';

describe('appMainUsersReducer', () => {
  it('returns the initial state', () => {
    expect(appMainUsersReducer(undefined, {})).toEqual(fromJS({}));
  });
});
