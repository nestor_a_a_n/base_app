/**
 *
 * Asynchronously loads the component for AppMainUsers
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
