import { createSelector } from 'reselect';

/**
 * Direct selector to the appMainList state domain
 */
const selectAppMainListDomain = (state) => state.get('appMainList');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AppMainList
 */

const makeSelectAppMainList = () => createSelector(
  selectAppMainListDomain,
  (substate) => substate.toJS()
);

export default makeSelectAppMainList;
export {
  selectAppMainListDomain,
};
