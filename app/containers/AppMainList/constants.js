/*
 *
 * AppMainList constants
 *
 */

export const DEFAULT_ACTION = 'app/AppMainList/DEFAULT_ACTION';

export const CHANGE_PROP = 'app/AppMainList/CHANGE_PROP';

export const CLEAR = 'app/AppMainList/CLEAR';

export const ATTEMP = 'app/AppMainList/ATTEMP';
export const SUCCESS = 'app/AppMainList/SUCCESS';
export const ERROR = 'app/AppMainList/ERROR';

export const DELETE_ATTEMP = 'app/AppMainList/DELETE_ATTEMP';
export const DELETE_SUCCESS = 'app/AppMainList/DELETE_SUCCESS';
export const DELETE_ERROR = 'app/AppMainList/DELETE_ERROR';
