/**
 *
 * AppMainList
 *
 */

import React from 'react';
import { Row } from 'reactstrap';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectAppMainList from './selectors';
import reducer from './reducer';
import saga from './saga';
// import messages from './messages';

// Actions
import { attempAction } from './actions';
import { addAction } from '../Modals/actions';

// Components
import TableUI from '../../components/Table';

// configs
import configAdmin from '../../configAdmin';
// import configTable from './configTable';

export class AppMainList extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.confirDelete = this.confirDelete.bind(this);
  }
  componentDidMount() {
    if (this.props.config.include) {
      window.location.hash = `#${configAdmin.restClient.apiQuery.buildUrlQuery({}, this.props.config.include)}`;
    } else {
      this.props.onGetList(window.location.hash, this.props.source);
    }
  }
  componentWillReceiveProps(newProps) {
    if (this.props.location.hash !== newProps.location.hash || this.props.source !== newProps.source) {
      this.props.onGetList(newProps.location.hash.replace('#', ''), newProps.source);
    }
  }
  confirDelete() {
    // console.log('confirDelete', item);
    this.props.onAddModal({
      title: 'Confirm',
      body: 'Do you really want to delete it?',
      footerButtoms: [
        { color: 'secondary', text: 'Cancel' },
        { color: 'primary', text: 'Yes' },
      ],
    });
  }
  render() {
    const { config, create, update, remove } = this.props;
    const { loading, list, paginate } = this.props.appmainList;
    return (
      <div className="animated fadeIn">
        <Row>
          <TableUI
            {...config}
            data={list}
            paginate={paginate}
            paginateVisible={5}
            loading={loading}
            callbackQuery={() => {}}
            history={this.props.history}
            create={create}
            update={update}
            remove={remove}
            confirDelete={this.confirDelete}
          />
        </Row>
        {/* <div style={{position: 'relative', display: 'flex'}}>
          <LoadingSpiner loading={true}/>
        </div> */}
      </div>
    );
  }
}

AppMainList.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  history: PropTypes.object,
  appmainList: PropTypes.object,
  onGetList: PropTypes.func,
  location: PropTypes.object,
  config: PropTypes.object,
  source: PropTypes.string,
  create: PropTypes.any,
  update: PropTypes.any,
  remove: PropTypes.any,
  onAddModal: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  appmainList: makeSelectAppMainList(),
});

function mapDispatchToProps(dispatch) {
  return {
    // dispatch,
    onGetList: (query, source) => dispatch(attempAction(query, source)),
    onAddModal: (configModal) => dispatch(addAction(configModal)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'appMainList', reducer });
const withSaga = injectSaga({ key: 'appMainList', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AppMainList);
