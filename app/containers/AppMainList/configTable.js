import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Badge, Button } from 'reactstrap';
import { USER_STATUS, USER_ROLE, PHALCON_REST_WHERE, PHALCON_REST_WHERE_LIKE, PHALCON_REST_WHERE_IS_EQUAL, TABLE_TAG_INPUT, TABLE_TAG_SELECT } from '../../utils/constants';
import usersMessages from '../../utils/messages/modals/users';
import messages from './messages';

export default {
  icon: 'fa fa-align-justify',
  title: <FormattedMessage {...messages.table_title} />,
  types: {
    hover: true,
    bordered: true,
    striped: false,
    responsive: true,
    size: 'sm',
  },
  search: true,
  sort: true,
  columns: [
    {
      name: <FormattedMessage {...usersMessages.USERNAME} />,
      key: 'username',
      search: {
        tag: TABLE_TAG_INPUT, // input, select
        parameter: PHALCON_REST_WHERE, // Parameter Query Phalcon Rest
        conditional: PHALCON_REST_WHERE_LIKE, // Conditional Query Phalcon Rest
      },
    },
    {
      name: <FormattedMessage {...usersMessages.DATE_REGISTERED} />,
      key: 'createAt',
      render: (value) => value, // value, row, colum, data, index
      search: {
        tag: TABLE_TAG_INPUT, // input, select
        parameter: PHALCON_REST_WHERE, // Parameter Query Phalcon Rest
        conditional: PHALCON_REST_WHERE_LIKE, // Conditional Query Phalcon Rest
      },
    },
    {
      name: <FormattedMessage {...usersMessages.ROLE} />,
      key: 'role',
      render: (value) => <FormattedMessage {...usersMessages[USER_ROLE[value]]} />, // value, row, colum, data, index
      search: {
        tag: TABLE_TAG_SELECT, // input, select
        parameter: PHALCON_REST_WHERE, // Parameter Query Phalcon Rest
        conditional: PHALCON_REST_WHERE_IS_EQUAL, // Conditional Query Phalcon Rest
        options: USER_ROLE,
        getOption(options, key) {
          return {
            text: <FormattedMessage {...usersMessages[options[key]]} />,
            value: key,
          };
        },
      },
    },
    {
      name: <FormattedMessage {...usersMessages.STATUS} />,
      key: 'status',
      render: (value) => (
        <Badge color={USER_STATUS[value].color}>
          <FormattedMessage {...usersMessages[USER_STATUS[value].name]} />
        </Badge>
      ), // value, row, colum, data, index
      search: {
        tag: TABLE_TAG_SELECT, // input, select
        parameter: PHALCON_REST_WHERE, // Parameter Query Phalcon Rest
        conditional: PHALCON_REST_WHERE_IS_EQUAL, // Conditional Query Phalcon Rest
        options: USER_STATUS,
        getOption(options, key) {
          return {
            text: <FormattedMessage {...usersMessages[options[key].name]} />,
            value: key,
          };
        },
      },
    },
    {
      name: '',
      key: 'edit-action',
      render: () => (
        <Button color="primary" size="sm" outline>
          <i className="fa fa-pencil"></i>
        </Button>
      ), // value, row, colum, data, index
    },
  ],
};
