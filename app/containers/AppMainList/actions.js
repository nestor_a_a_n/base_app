/*
 *
 * AppMainUsers actions
 *
 */

import {
  DEFAULT_ACTION,
  CHANGE_PROP,
  CLEAR,
  ATTEMP,
  SUCCESS,
  ERROR,
  DELETE_ATTEMP,
  DELETE_SUCCESS,
  DELETE_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function changePropAction(prop, value) {
  return {
    type: CHANGE_PROP,
    prop,
    value,
  };
}

export function clearAction() {
  return {
    type: CLEAR,
  };
}

export function attempAction(query, source) {
  return {
    type: ATTEMP,
    query,
    source,
  };
}

export function successAction(list, paginate) {
  return {
    type: SUCCESS,
    list,
    paginate,
  };
}

export function errorAction(error) {
  return {
    type: ERROR,
    error,
  };
}

export function deleteAttempAction(id, source) {
  return {
    type: DELETE_ATTEMP,
    id,
    source,
  };
}

export function deleteSuccessAction(payload) {
  return {
    type: DELETE_SUCCESS,
    payload,
  };
}

export function deleteErrorAction(error) {
  return {
    type: DELETE_ERROR,
    error,
  };
}
