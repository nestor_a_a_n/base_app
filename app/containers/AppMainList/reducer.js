/*
 *
 * AppMainList reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  CHANGE_PROP,
  CLEAR,
  ATTEMP,
  SUCCESS,
  ERROR,
  DELETE_ATTEMP,
  DELETE_SUCCESS,
  DELETE_ERROR,
} from './constants';

const initialState = fromJS({
  loading: false,
  success: false,
  error: false,
  source: null,
  list: [],
  paginate: null,
});

function appMainListReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case CLEAR:
      return initialState;
    case CHANGE_PROP:
      return state.set(action.prop, action.value);
    case ATTEMP:
      return state
      .set('loading', true)
      .set('success', false)
      .set('error', false);
    case SUCCESS:
      return state
      .set('loading', false)
      .set('success', true)
      .set('list', action.list)
      .set('paginate', action.paginate || null);
    case ERROR:
      return state
      .set('loading', false)
      .set('error', action.error)
      .set('list', fromJS([]))
      .set('paginate', fromJS(action.paginate || null));
    case DELETE_ATTEMP:
      return state
      .set('loading', true)
      .set('success', false)
      .set('error', false);
    case DELETE_SUCCESS:
      return state
      .set('loading', false)
      .set('success', true)
      .set('error', false);
    case DELETE_ERROR:
      return state
      .set('loading', false)
      .set('success', false)
      .set('error', action.error);
    default:
      return state;
  }
}

export default appMainListReducer;
