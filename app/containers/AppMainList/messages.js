/*
 * AppMainList Messages
 *
 * This contains all the text for the AppMainList component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AppMainList.header',
    defaultMessage: 'This is AppMainList container !',
  },
  table_title: {
    id: 'app.containers.AppMainList.table_title',
    defaultMessage: 'Lista de usuarios',
  },
});
