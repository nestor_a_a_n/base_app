import { fromJS } from 'immutable';
import page500Reducer from '../reducer';

describe('page500Reducer', () => {
  it('returns the initial state', () => {
    expect(page500Reducer(undefined, {})).toEqual(fromJS({}));
  });
});
