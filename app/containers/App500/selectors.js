import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the page500 state domain
 */

const selectPage500Domain = state => state.get('page500', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by Page500
 */

const makeSelectPage500 = () =>
  createSelector(selectPage500Domain, substate => substate.toJS());

export default makeSelectPage500;
export { selectPage500Domain };
