/**
 *
 * Page500
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectPage500 from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {Container, Row, Col, Button, Input, InputGroup, InputGroupAddon, InputGroupButton} from "reactstrap";


/* eslint-disable react/prefer-stateless-function */
export class Page500 extends React.PureComponent {
  render() {
    // return (
    //   <div>
    //     <FormattedMessage {...messages.header} />
    //   </div>
    // );
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <span className="clearfix">
                <h1 className="float-left display-3 mr-4">500</h1>
                <h4 className="pt-3"><FormattedMessage {...messages.problem_title} /></h4>
                <p className="text-muted float-left"><FormattedMessage {...messages.problem_paragraph} /></p>
              </span>
              <InputGroup className="input-prepend">
                <InputGroupAddon><i className="fa fa-search"></i></InputGroupAddon>
                <Input size="16" type="text" placeholder="What are you looking for?" />
                <InputGroupButton>
                  <Button color="info"><FormattedMessage {...messages.search} /></Button>
                </InputGroupButton>
              </InputGroup>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

Page500.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  page500: makeSelectPage500(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'page500', reducer });
const withSaga = injectSaga({ key: 'page500', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Page500);
