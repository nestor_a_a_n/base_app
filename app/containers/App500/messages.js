/*
 * Page500 Messages
 *
 * This contains all the text for the Page500 component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  problem_title: {
    id: 'containers.app.500.problem_title',
    defaultMessage: 'Houston, we have a problem!',
  },
  problem_paragraph: {
    id: 'containers.app.500.problem_paragraph',
    defaultMessage: 'The page you are looking for is temporarily unavailable.',
  },
  search: {
    id: 'containers.app.500.search',
    defaultMessage: 'Search',
  },
});
