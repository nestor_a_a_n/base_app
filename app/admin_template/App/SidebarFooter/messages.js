/*
 * SidebarFooter Messages
 *
 * This contains all the text for the SidebarFooter component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SidebarFooter.header',
    defaultMessage: 'This is the SidebarFooter component !',
  },
});
