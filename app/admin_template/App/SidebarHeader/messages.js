/*
 * SidebarHeader Messages
 *
 * This contains all the text for the SidebarHeader component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SidebarHeader.header',
    defaultMessage: 'This is the SidebarHeader component !',
  },
});
