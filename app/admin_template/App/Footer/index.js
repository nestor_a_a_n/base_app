import React from 'react';
// import { FormattedMessage } from 'react-intl';

// import LocaleToggle from 'containers/ContainerLocaleToggle';
// import Wrapper from './Wrapper';
// import messages from './messages';

function Footer() {  
  return (
    <footer className="app-footer">
      <a href="#">App</a> &copy; 2018 Nestor Andres A. A.
      <span className="float-right">Powered by <a href="#">React Js</a></span>
    </footer>
  )
}

export default Footer;
