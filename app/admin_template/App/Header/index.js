import React from 'react';
import {
  Badge,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink as RsNavLink,
  NavbarToggler,
  NavbarBrand,
  DropdownToggle,
  Progress,
} from 'reactstrap';
import { NavLink } from 'react-router-dom';
// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
class Header extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false,
      dropdownOpenBell: false,
      dropdownOpenList: false,
      dropdownOpenEnvelope: false,
    };
  }
  toggle(prop) {
    this.state[prop] = !this.state[prop];
    this.setState(this.state);
  }
  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }
  sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-minimized');
  }
  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }
  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('aside-menu-hidden');
  }
  render() {
    return (
      <header className="app-header navbar">
        <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}><span style={{ color: '#000000' }}>&#9776;</span></NavbarToggler>
        <NavbarBrand href="#"></NavbarBrand>
        <NavbarToggler className="d-md-down-none" onClick={this.sidebarToggle}><span style={{ color: '#000000' }}>&#9776;</span></NavbarToggler>
        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/">Ir Pagina de inicio</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink to="/widgets">Widgets</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <RsNavLink href="#">Settings</RsNavLink>
          </NavItem>
        </Nav>
        <Nav className="ml-auto" navbar>
          <Dropdown isOpen={this.state.dropdownOpenBell} toggle={() => { this.toggle('dropdownOpenBell'); }} tag="li" className="d-md-down-none nav-item">
            <DropdownToggle tag="a" className="nav-link" href="#">
              <i className="icon-bell"></i><Badge pill color="danger">5</Badge>
            </DropdownToggle>
            <DropdownMenu right className={this.state.dropdownOpenBell ? 'show' : ''}>
              <DropdownItem header tag="div" className="text-center"><strong>You have 5 notifications</strong></DropdownItem>
              <DropdownItem >
                <i className="icon-user-follow text-success"></i> New user registered
              </DropdownItem>
              <DropdownItem >
                <i className="icon-user-unfollow text-danger"></i> User deleted
              </DropdownItem>
              <DropdownItem >
                <i className="icon-chart text-info"></i> Sales report is ready
              </DropdownItem>
              <DropdownItem >
                <i className="icon-basket-loaded text-primary"></i> New client
              </DropdownItem>
              <DropdownItem >
                <i className="icon-speedometer text-warning"></i> Server overloaded
              </DropdownItem>
              <DropdownItem header tag="div" className="text-center"><strong>Server</strong></DropdownItem>
              <DropdownItem>
                <div className="text-uppercase mb-1">
                  <small><b>CPU Usage</b></small>
                </div>
                <Progress className="progress-xs mb-0" color="info" value={25} />
                <small className="text-muted">348 Processes. 1/4 Cores.</small>
              </DropdownItem>
              <DropdownItem>
                <div className="text-uppercase mb-1">
                  <small><b>Memory Usage</b></small>
                </div>
                <Progress className="progress-xs mb-0" color="warning" value={70} />
                <small className="text-muted">11444GB/16384MB.</small>
              </DropdownItem>
              <DropdownItem>
                <div className="text-uppercase mb-1">
                  <small><b>SSD 1 Usage</b></small>
                </div>
                <Progress className="progress-xs mb-0" color="danger" value={90} />
                <small className="text-muted">243GB/256GB.</small>
              </DropdownItem>
            </DropdownMenu>
          </Dropdown>
          <Dropdown isOpen={this.state.dropdownOpenList} toggle={() => { this.toggle('dropdownOpenList'); }} tag="li" className="d-md-down-none nav-item">
            <DropdownToggle tag="a" className="nav-link" href="#">
              <i className="icon-list"></i><Badge pill color="warning">15</Badge>
            </DropdownToggle>
            <DropdownMenu right className={this.state.dropdownOpenList ? 'show' : ''}>
              <DropdownItem header tag="div" className="text-center"><strong>You have 15 pending tasks</strong></DropdownItem>
              <DropdownItem>
                <div className="small mb-1">
                  <small>Upgrade NPM &amp; Bower </small>
                  <span className="float-right"><strong>0%</strong></span>
                </div>
                <Progress className="progress-xs" color="info" value={0} />
              </DropdownItem>
              <DropdownItem>
                <div className="small mb-1">
                  <small>ReactJS Version</small>
                  <span className="float-right"><strong>25%</strong></span>
                </div>
                <Progress className="progress-xs" color="damger" value={25} />
              </DropdownItem>
              <DropdownItem>
                <div className="small mb-1">
                  <small>VueJS Version</small>
                  <span className="float-right"><strong>50%</strong></span>
                </div>
                <Progress className="progress-xs" color="warning" value={50} />
              </DropdownItem>
              <DropdownItem>
                <div className="small mb-1">
                  <small>Add new layouts</small>
                  <span className="float-right"><strong>75%</strong></span>
                </div>
                <Progress className="progress-xs" color="info" value={75} />
              </DropdownItem>
              <DropdownItem>
                <div className="small mb-1">
                  <small>Angular 2 Cli Version </small>
                  <span className="float-right"><strong>100%</strong></span>
                </div>
                <Progress className="progress-xs" color="success" value={100} />
              </DropdownItem>
              <DropdownItem header tag="div" className="text-center"><strong>View all tasks</strong></DropdownItem>
            </DropdownMenu>
          </Dropdown>

          <Dropdown isOpen={this.state.dropdownOpenEnvelope} toggle={() => { this.toggle('dropdownOpenEnvelope'); }} tag="li" className="d-md-down-none nav-item">
            <DropdownToggle tag="a" className="nav-link" href="#">
              <i className="icon-envelope-letter"></i><Badge pill color="info">7</Badge>
            </DropdownToggle>
            <DropdownMenu right className={`dropdown-menu-lg ${this.state.dropdownOpenEnvelope ? 'show' : ''}`}>
              <DropdownItem header tag="div" className="text-center"><strong>You have 7 messages</strong></DropdownItem>
              <DropdownItem tag="a">
                <div className="message">
                  <div className="pt-3 mr-3 float-left">
                    <div className="avatar">
                      <img src="http://localhost:3000/images/avatars/7.jpg" className="img-avatar" alt="admin@bootstrapmaster.com" />
                      <span className="avatar-status badge-success"></span>
                    </div>
                  </div>
                  <div>
                    <small className="text-muted">John Doe</small>
                    <small className="text-muted float-right mt-1">Just now</small>
                  </div>
                  <div className="text-truncate font-weight-bold">
                    <span className="fa fa-exclamation text-danger"></span> Important message
                  </div>
                  <div className="small text-muted text-truncate">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...
                  </div>
                </div>
              </DropdownItem>
              <DropdownItem tag="a">
                <div className="message">
                  <div className="pt-3 mr-3 float-left">
                    <div className="avatar">
                      <img src="http://localhost:3000/images/avatars/7.jpg" className="img-avatar" alt="admin@bootstrapmaster.com" />
                      <span className="avatar-status badge-warning"></span>
                    </div>
                  </div>
                  <div>
                    <small className="text-muted">John Doe</small>
                    <small className="text-muted float-right mt-1">Just now</small>
                  </div>
                  <div className="text-truncate font-weight-bold">
                     Common
                  </div>
                  <div className="small text-muted text-truncate">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...
                  </div>
                </div>
              </DropdownItem>

              <DropdownItem tag="a">
                <div className="message">
                  <div className="pt-3 mr-3 float-left">
                    <div className="avatar">
                      <img src="http://localhost:3000/images/avatars/7.jpg" className="img-avatar" alt="admin@bootstrapmaster.com" />
                      <span className="avatar-status badge-danger"></span>
                    </div>
                  </div>
                  <div>
                    <small className="text-muted">John Doe</small>
                    <small className="text-muted float-right mt-1">Just now</small>
                  </div>
                  <div className="text-truncate font-weight-bold">
                     Common
                  </div>
                  <div className="small text-muted text-truncate">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...
                  </div>
                </div>
              </DropdownItem>
              <DropdownItem tag="a">
                <div className="message">
                  <div className="pt-3 mr-3 float-left">
                    <div className="avatar">
                      <img src="http://localhost:3000/images/avatars/7.jpg" className="img-avatar" alt="admin@bootstrapmaster.com" />
                      <span className="avatar-status badge-info"></span>
                    </div>
                  </div>
                  <div>
                    <small className="text-muted">John Doe</small>
                    <small className="text-muted float-right mt-1">Just now</small>
                  </div>
                  <div className="text-truncate font-weight-bold">
                     Common
                  </div>
                  <div className="small text-muted text-truncate">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...
                  </div>
                </div>
              </DropdownItem>
              <DropdownItem header tag="div" className="text-center"><strong>View all messages</strong></DropdownItem>
            </DropdownMenu>
          </Dropdown>

          <NavItem className="d-md-down-none">
            <RsNavLink href="#"><i className="icon-location-pin"></i></RsNavLink>
          </NavItem>

          <Dropdown isOpen={this.state.dropdownOpen} toggle={() => { this.toggle('dropdownOpen'); }} tag="li" className="d-md-down-none nav-item">
            <DropdownToggle tag="a" className="nav-link" href="#">
              <img src={'http://localhost:3000/images/avatars/6.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
            </DropdownToggle>
            <DropdownMenu right className={this.state.dropdownOpen ? 'show' : ''}>
              <DropdownItem header tag="div" className="text-center"><strong>Cuenta</strong></DropdownItem>
              <DropdownItem><i className="fa fa-lock"></i> Logout</DropdownItem>
              <DropdownItem><i className="fa fa-bell-o"></i> Updates<Badge color="info">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-envelope-o"></i> Messages<Badge color="success">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-tasks"></i> Tasks<Badge color="danger">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-comments"></i> Comments<Badge color="warning">42</Badge></DropdownItem>
              <DropdownItem header tag="div" className="text-center"><strong>Settings</strong></DropdownItem>
              <DropdownItem><i className="fa fa-user"></i> Profile</DropdownItem>
              <DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>
              <DropdownItem><i className="fa fa-usd"></i> Payments<Badge color="secondary">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-file"></i> Projects<Badge color="primary">42</Badge></DropdownItem>
              <DropdownItem divider />
              <DropdownItem><i className="fa fa-shield"></i> Lock Account</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </Nav>
        <NavbarToggler className="d-md-down-none" type="button" onClick={this.asideToggle}>&#9776;</NavbarToggler>
      </header>
    );
  }
}

export default Header;
