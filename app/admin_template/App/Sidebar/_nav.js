export default {
  items: [
    {
      title: true,
      name: 'UI elements',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Components',
      url: '/template/components',
      icon: 'icon-puzzle',
      children: [
        {
          name: 'Buttons',
          url: '/template/components/buttons',
          icon: 'icon-puzzle'
        },
        {
          name: 'Social Buttons',
          url: '/template/components/social-buttons',
          icon: 'icon-puzzle'
        },
        {
          name: 'Cards',
          url: '/template/components/cards',
          icon: 'icon-puzzle'
        },
        {
          name: 'Forms',
          url: '/template/components/forms',
          icon: 'icon-puzzle'
        },
        {
          name: 'Modals',
          url: '/template/components/modals',
          icon: 'icon-puzzle'
        },
        {
          name: 'Switches',
          url: '/template/components/switches',
          icon: 'icon-puzzle'
        },
        {
          name: 'Tables',
          url: '/template/components/tables',
          icon: 'icon-puzzle'
        },
        {
          name: 'Tabs',
          url: '/template/components/tabs',
          icon: 'icon-puzzle'
        }
      ]
    },
    {
      name: 'Icons',
      url: '/template/icons',
      icon: 'icon-star',
      children: [
        {
          name: 'Font Awesome',
          url: '/template/icons/font-awesome',
          icon: 'icon-star',
          badge: {
            variant: 'secondary',
            text: '4.7'
          }
        },
        {
          name: 'Simple Line Icons',
          url: '/template/icons/simple-line-icons',
          icon: 'icon-star'
        }
      ]
    },
    {
      name: 'Widgets',
      url: '/template/widgets',
      icon: 'icon-calculator',
      badge: {
        variant: 'info',
        text: 'NEW'
      }
    },
    {
      name: 'Charts',
      url: '/template/charts',
      icon: 'icon-pie-chart'
    },
    {
      divider: true
    },
    {
      title: true,
      name: 'Extras'
    },
    {
      name: 'Pages',
      url: '/template/pages',
      icon: 'icon-star',
      children: [
        {
          name: 'Login',
          url: '/template/pages/login',
          icon: 'icon-star'
        },
        {
          name: 'Register',
          url: '/template/pages/register',
          icon: 'icon-star'
        },
        {
          name: 'Error 404',
          url: '/template/pages/404',
          icon: 'icon-star'
        },
        {
          name: 'Error 500',
          url: '/template/pages/500',
          icon: 'icon-star'
        }
      ]
    },
    // {
    //   name: 'Download CoreUI',
    //   url: 'http://coreui.io/react/',
    //   icon: 'icon-cloud-download',
    //   class: 'mt-auto',
    //   variant: 'success'
    // },
    // {
    //   name: 'Try CoreUI PRO',
    //   url: 'http://coreui.io/pro/react/',
    //   icon: 'icon-layers',
    //   variant: 'danger'
    // }
  ]
};
