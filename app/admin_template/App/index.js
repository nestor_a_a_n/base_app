/**
 *
 * AppMain
 *
 */

import React from 'react';
import {
  Container
} from 'reactstrap';
import { Helmet } from 'react-helmet';
import { Switch, Route, Redirect } from 'react-router-dom';

// Components
import Header from './Header';
import Footer from './Footer';
import Sidebar from './Sidebar';
import Aside from './Aside'

// Pages
import PageDashboard from '../Dashboard';
import ComponentsButtons from '../Components/Buttons';
import ComponentsSocialButtons from '../Components/SocialButtons';
import ComponentsCards from '../Components/Cards';
import ComponentsForms from '../Components/Forms';
import ComponentsModals from '../Components/Modals';
import ComponentsSwitches from '../Components/Switches';
import ComponentsTables from '../Components/Tables';
import ComponentsTabs from '../Components/Tabs';
import IconsFontAwesome from '../Icons/FontAwesome';
import IconsSimpleLineIcons from '../Icons/SimpleLineIcons';
import Widgets from '../Widgets';
import Charts from '../Charts';
// import App500 from 'containers/App500/Loadable';


/* eslint-disable react/prefer-stateless-function */
export default class MainApp extends React.PureComponent {
  render() {
    return (
      <div className='app'>
        <Helmet
          titleTemplate="%s - React.js Boilerplate"
          defaultTitle="React.js Boilerplate"
        >
          <meta
            name="description"
            content="A React.js Boilerplate application"
          />
        </Helmet>
        <Header />
        <div className="app-body"> 
          <Sidebar {...this.props}/>
          <main className="main"> 
            <Container fluid>
              <Switch>
                <Route path="/template/components/buttons" component={ComponentsButtons} />
                <Route path="/template/components/social-buttons" component={ComponentsSocialButtons} />
                <Route path="/template/components/cards" component={ComponentsCards} />
                <Route path="/template/components/forms" component={ComponentsForms} />
                <Route path="/template/components/modals" component={ComponentsModals} />
                <Route path="/template/components/switches" component={ComponentsSwitches} />
                <Route path="/template/components/tables" component={ComponentsTables} />
                <Route path="/template/components/tabs" component={ComponentsTabs} />
                <Route path="/template/icons/font-awesome" component={IconsFontAwesome} />
                <Route path="/template/icons/simple-line-icons" component={IconsSimpleLineIcons} />
                <Route path="/template/widgets" component={Widgets} />
                <Route path="/template/charts" component={Charts} />
                <Route path="/template" component={PageDashboard} />
              </Switch>
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
    )
  }
}