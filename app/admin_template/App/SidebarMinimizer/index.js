/**
 *
 * SidebarMinimizer
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
class SidebarMinimizer extends React.Component {
  sidebarMinimize() {
    document.body.classList.toggle('sidebar-minimized');
  }
  brandMinimize() {
    document.body.classList.toggle('brand-minimized');
  }
  render() {
    return (
      <button className="sidebar-minimizer mt-auto" type="button" onClick={(event) => { this.sidebarMinimize(); this.brandMinimize() }}></button>
    )
  }
}

SidebarMinimizer.propTypes = {};

export default SidebarMinimizer;
