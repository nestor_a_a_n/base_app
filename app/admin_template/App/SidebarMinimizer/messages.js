/*
 * SidebarMinimizer Messages
 *
 * This contains all the text for the SidebarMinimizer component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SidebarMinimizer.header',
    defaultMessage: 'This is the SidebarMinimizer component !',
  },
});
