/**
*
* Alert
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Alert as AlertBootstrap } from 'reactstrap';
// import styled from 'styled-components';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

function Alert({ color, message }) {
  // return (
  //   <div>
  //     <FormattedMessage {...messages.header} />
  //   </div>
  // );
  if (!message) {
    return null;
  }
  return (
    <AlertBootstrap color={color}>
      {message}
    </AlertBootstrap>
  );
}

Alert.propTypes = {
  message: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  color: PropTypes.oneOf(['info', 'warning', 'danger', 'success']),
};

Alert.defaultProp = {
  message: '',
  color: 'success',
};

export default Alert;
