/*
 * Alert Messages
 *
 * This contains all the text for the Alert component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Alert.header',
    defaultMessage: 'This is the Alert component !',
  },
});
