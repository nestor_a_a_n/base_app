import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import messages from './messages';

// Utils
import apiQuery from '../../utils/miscellaneous/adapterQuery';

/* eslint-disable react/prefer-stateless-function */
class Paginate extends React.Component {
  // constructor(props) {
  //   super(props);
  // }
  render() {
    const { total, page, limit, visible } = this.props;
    if (!page) {
      return null;
    }

    const items = [];
    let y = Math.floor(visible / 2);
    let derecha = Math.min(total, page + y);
    if ((page + y) > total) {
      y += (page + y) - total;
    }
    if ((visible % 2) === 0) {
      y -= 1;
    }
    const izquierda = Math.max(1, page - y);
    while (((derecha - izquierda) + 1) < visible) {
      derecha += 1;
    }
    for (let i = izquierda; i <= derecha; i += 1) {
      if (i <= total) {
        items.push(
          <PaginationItem active={i === page} key={`paginate-item-${i}`}>
            <PaginationLink href={`#${apiQuery.buildUrlQuery({ page: { value: i }, limit: { value: limit } })}`}>
              {i}
            </PaginationLink>
          </PaginationItem>
        );
      }
    }

    return (
      <Pagination className="float-right">
        <PaginationItem disabled={page === 1}>
          <PaginationLink previous href={`#${apiQuery.buildUrlQuery({ page: { value: 1 }, limit: { value: limit } })}`}>
            <FormattedMessage {...messages.first} />
          </PaginationLink>
        </PaginationItem>
        {/* <PaginationItem disabled={page === 1}>
          <PaginationLink previous href={`#${buildQuery({ page: (page-1), limit })}`}>
            <FormattedMessage {...messages.prev} />
          </PaginationLink>
        </PaginationItem> */}
        {items}
        {/* <PaginationItem disabled={page === total}>
          <PaginationLink next href={`#${buildQuery({ page: (page+1), limit })}`}>
            <FormattedMessage {...messages.next} />
          </PaginationLink>
        </PaginationItem> */}
        <PaginationItem disabled={page === total}>
          <PaginationLink next href={`#${apiQuery.buildUrlQuery({ page: { value: total }, limit: { value: limit } })}`}>
            <FormattedMessage {...messages.last} />
          </PaginationLink>
        </PaginationItem>
      </Pagination>
    );
  }
}
Paginate.propTypes = {
  total: PropTypes.number,
  limit: PropTypes.number,
  page: PropTypes.number,
  visible: PropTypes.number,
};

export default Paginate;
