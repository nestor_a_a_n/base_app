/*
 * Table Messages
 *
 * This contains all the text for the Table component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Table.header',
    defaultMessage: 'This is the Table component !',
  },
  first: {
    id: 'app.components.Table.first',
    defaultMessage: 'Primero',
  },
  prev: {
    id: 'app.components.Table.prev',
    defaultMessage: 'Previo',
  },
  next: {
    id: 'app.components.Table.next',
    defaultMessage: 'Siguiente',
  },
  last: {
    id: 'app.components.Table.last',
    defaultMessage: 'Ultimo',
  },
  all: {
    id: 'app.components.Table.all',
    defaultMessage: 'Todo',
  },
  of: {
    id: 'app.components.Table.of',
    defaultMessage: 'de',
  },
});
