/**
*
* Table
*
*/

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { Row, Col, Card, CardHeader, CardBlock, Table as TableTrap, Input, Pagination, PaginationItem, PaginationLink, Button } from 'reactstrap';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

// Components
import LoadingSpiner from '../LoadingSpiner';
import Paginate from './Paginate';

// Utils
import configAdmin from '../../configAdmin';

import { TABLE_TAG_INPUT, TABLE_TAG_SELECT } from '../../utils/constants';

const { restClient } = configAdmin;

const searchTag = (column, include) => {
  const { apiQuery } = restClient;
  if (!column.search) {
    return null;
  }
  const { key } = column;
  const { tag, options, textField, valueField } = column.search;
  if (tag === TABLE_TAG_INPUT) {
    return (
      <Input
        className="form-control-sm"
        value={apiQuery.getValueQuery({ ...column.search, property: key })}
        onChange={(evt) => {
          const q = {};
          q[key] = column.search;
          q[key].value = evt.target.value;
          window.location.hash = `#${apiQuery.buildUrlQuery(q, include)}`;
        }}
      />
    );
  } else if (tag === TABLE_TAG_SELECT) {
    let list = [];
    if (typeof options === 'object' && (typeof textField === 'function' || typeof textField === 'string') && typeof valueField === 'string') {
      list = options.map((option) => {
        let text = '';
        if (typeof textField === 'function') {
          text = textField(option);
        } else {
          text = option[textField];
        }
        return (
          <option key={`option-${key}-${option[valueField]}`} value={option[valueField]}>{text}</option>
        );
      });
    }
    return (
      <Input
        type="select"
        className="form-control-sm"
        value={apiQuery.getValueQuery({ ...column.search, property: key })}
        onChange={(evt) => {
          const q = {};
          q[key] = column.search;
          q[key].value = evt.target.value;
          window.location.hash = `#${apiQuery.buildUrlQuery(q, include)}`;
        }}
      >
        <option key={`option-${key}-all`} value={''}></option>
        {list}
      </Input>
    );
  }
  return null;
};

// const ButtomLimit = (actualLimit, newLimit) => (
//   <Button
//     outline={actualLimit !== newLimit}
//     color="primary"
//     size="sm"
//     onClick={() => {
//       window.location.hash = `#${restClient.apiQuery.buildUrlQuery({
//         limit: { value: newLimit },
//         page: { value: 1 },
//       })}`;
//     }}
//   >
//     {newLimit}
//   </Button>
// );

const renderShort = (short, column, include) => {
  if (!short) {
    return null;
  }
  if (column.name === '') {
    return null;
  }
  const oldValue = restClient.apiQuery.getValueQuery({ parameter: 'sort', property: column.key });
  let classIcon = 'fa fa-sort';
  if (oldValue === '-1') {
    classIcon = 'fa fa-sort-desc';
  } else if (oldValue === '1') {
    classIcon = 'fa fa-sort-asc';
  }
  const disabled = include === column.key;
  return (
    <div className="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
      <div className="btn-group mr-2" role="group" aria-label="First group">
        <Button
          outline
          color={disabled ? 'secondary' : 'primary'}
          size="sm"
          disabled={disabled}
          onClick={() => {
            const newValue = String(restClient.apiQuery.getValueQuery({ parameter: 'sort', property: column.key }));
            const q = {};
            q[column.key] = { parameter: 'sort' };
            if (newValue === '') {
              q[column.key].value = '-1';
            } else if (newValue === '-1') {
              q[column.key].value = '1';
            } else {
              q[column.key].value = '';
            }
            window.location.hash = `#${restClient.apiQuery.buildUrlQuery(q, include)}`;
          }}
        >
          <i className={classIcon} />
        </Button>
      </div>
    </div>
  );
};

class Table extends React.Component { // eslint-disable-line react/prefer-stateless-function
  // constructor(props) {
  //   super(props);
  // }
  infoPaginate({ page, limit, totalItems }) {
    let limitMin = ((page - 1) * limit) + 1;
    let limitMax = (page) * limit;
    if (limitMin > totalItems) {
      limitMin = totalItems;
    }
    if (limitMax > totalItems) {
      limitMax = totalItems;
    }
    return `${limitMin}-${limitMax} `;
  }
  render() {
    const { icon, title, types, sort, search, sources, data, paginate, paginateVisible, loading, create, update, remove, include, confirDelete } = this.props;
    return (
      <Col>
        <Card>
          <CardHeader>
            <i className={icon}></i>
            {title}
            <div className="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
              {/* <div className="btn-group mr-2" role="group" aria-label="First group">
                {ButtomLimit(limit, '5')}
                {ButtomLimit(limit, '10')}
                {ButtomLimit(limit, '25')}
                {ButtomLimit(limit, '50')}
              </div> */}
              <div className="btn-group mr-3" role="group" aria-label="Second group">
                {create
                ? <Button outline color="primary" size="sm">
                  Crear usuario
                </Button>
                : null}
              </div>
            </div>
          </CardHeader>
          <CardBlock className="card-body">
            <LoadingSpiner loading={loading} />
            <TableTrap {...types}>
              <thead>
                <tr>
                  {sources.map((source) => <th key={`head-${source.key}`}>{source.name || source.key}{renderShort(sort, source, include)}</th>)}
                  {update || remove
                  ? <th></th>
                  : null}
                </tr>
              </thead>
              <tbody>
                {search
                ? <tr>
                  {sources.map((column) => <td key={`head-search-${column.key}`}>{searchTag(column, include)}</td>)}
                  {update || remove
                  ? <td></td>
                  : null}
                </tr>
                : null}
                {data.map((row, index) => (
                  <tr key={`row-${row.id}`}>
                    {sources.map((column) => {
                      const { render } = column;
                      let html = null;
                      if (typeof row[column.key] !== 'undefined') {
                        html = row[column.key];
                        if (typeof render === 'function') {
                          html = render(html, row, column, data, index);
                        }
                      }
                      return (
                        <td key={`row-${row.id}-${column.key}`}>{html}</td>
                      );
                    })}
                    {update || remove
                    ? <td className="float-right">
                      <div className="btn-group mr-3" role="group" aria-label="Second group">
                        {update
                        ? <Button color="primary" size="sm" outline>
                          <i className="fa fa-pencil"></i>
                        </Button>
                        : null}
                        {remove
                        ? <Button color="danger" size="sm" outline onClick={() => confirDelete(row)} >
                          <i className="fa fa-remove"></i>
                        </Button>
                        : null}
                      </div>
                    </td>
                    : null}
                  </tr>
                  )
                )}
              </tbody>
            </TableTrap>
            {paginate
            ? <Row>
              <Col xs="12" sm="6">
                <Pagination>
                  <PaginationItem disabled>
                    <PaginationLink >
                      {this.infoPaginate(paginate)}
                      <FormattedMessage {...messages.of} />
                      {` ${paginate.totalItems}`}
                    </PaginationLink>
                  </PaginationItem>
                </Pagination>
              </Col>
              <Col xs="12" sm="6">
                <Paginate {...paginate} visible={paginateVisible} />
              </Col>
            </Row>
            : null }
          </CardBlock>
        </Card>
      </Col>
    );
  }
}

Table.propTypes = {
  icon: PropTypes.string,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  loading: PropTypes.bool,
  types: PropTypes.shape({
    hover: PropTypes.bool,
    bordered: PropTypes.bool,
    striped: PropTypes.bool,
    responsive: PropTypes.bool,
    size: PropTypes.oneOf(['xl', 'sm']),
  }),
  search: PropTypes.bool,
  sort: PropTypes.bool,
  sources: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    key: PropTypes.string.isRequired,
    render: PropTypes.func,
  })).isRequired,
  data: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  })),
  paginate: PropTypes.any,
  paginateVisible: PropTypes.number,
  create: PropTypes.any,
  update: PropTypes.any,
  remove: PropTypes.any,
  include: PropTypes.any,
  confirDelete: PropTypes.func,
};

export default Table;
