/**
*
* Loading
*
*/

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

function LoadingSpiner({ loading }) {
  // return (
  //   <div>
  //     <FormattedMessage {...messages.header} />
  //   </div>
  // );
  // <div id="loading-bar">
  //   <div class="bar">
  //       <div class="peg"></div>
  //   </div>
  // </div>
  if (!loading) {
    return null;
  }
  // return (
  //   <div className="outer" >
  //     <div className="inner">
  //       <div id="loading-bar-spinner">
  //         <div className="spinner-icon icons font-5xl d-block mt-4"></div>
  //       </div>
  //     </div>
  //   </div>
  // )
  return (
    <div className="waiting">
      <div className="v-align-container">
        <div className="v-align-outer">
          <div className="v-align-inner">
            <div className="v-align-centered">
              <div id="loading-bar-spinner" style={{ position: null }}>
                <div className="spinner-icon icons font-5xl d-block mt-4"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
  // return (
  //   <div className="loading-screen">
  //     <div id="loading-bar-spinner">
  //       <div className="spinner-icon"></div>
  //     </div>
  //   </div>
  // )
}

LoadingSpiner.propTypes = {
  loading: PropTypes.bool,
};

LoadingSpiner.defaultProps = {
  loading: false,
};

export default LoadingSpiner;
