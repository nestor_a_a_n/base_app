import React from 'react';
import PropTypes from 'prop-types';
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Nav,
  // NavItem,
  // NavLink as RsNavLink,
  NavbarToggler,
  NavbarBrand,
} from 'reactstrap';
// import {NavLink} from 'react-router-dom';
// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
class Header extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false,
      dropdownOpenBell: false,
      dropdownOpenList: false,
      dropdownOpenEnvelope: false,
    };
  }
  toggle(prop) {
    this.state[prop] = !this.state[prop];
    this.setState(this.state);
  }
  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }
  sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-minimized');
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }
  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('aside-menu-hidden');
  }
  render() {
    return (
      <header className="app-header navbar">
        <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}><span style={{ color: '#000000' }}>&#9776;</span></NavbarToggler>
        <NavbarBrand href="#"></NavbarBrand>
        <NavbarToggler className="d-md-down-none" onClick={this.sidebarToggle}><span style={{ color: '#000000' }}>&#9776;</span></NavbarToggler>
        <Nav className="d-md-down-none" navbar>
        </Nav>
        <Nav className="ml-auto" navbar>
          <Dropdown isOpen={this.state.dropdownOpen} toggle={() => { this.toggle('dropdownOpen'); }} tag="li" className="d-md-down-none nav-item">
            <DropdownToggle tag="a" className="nav-link" href="#">
              <img src={'http://localhost:3000/images/avatars/6.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
            </DropdownToggle>
            <DropdownMenu right className={this.state.dropdownOpen ? 'show' : ''}>
              <DropdownItem header tag="div" className="text-center"><strong>Cuenta</strong></DropdownItem>
              <DropdownItem onClick={this.props.onLogout}><i className="fa fa-lock"></i> Logout</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </Nav>
      </header>
    );
  }
}
Header.propTypes = {
  onLogout: PropTypes.func,
};

export default Header;
