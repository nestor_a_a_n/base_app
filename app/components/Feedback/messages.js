/*
 * Feedback Messages
 *
 * This contains all the text for the Feedback component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Feedback.header',
    defaultMessage: 'This is the Feedback component !',
  },
});
