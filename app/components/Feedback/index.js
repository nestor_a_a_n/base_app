/**
*
* Feedback
*
*/

import React from 'react';
import PropTypes from 'prop-types';

const getFeedback = (message, prop) => {
  if (typeof message !== 'string') {
    return [];
  }
  const objMsg = JSON.parse(message);
  if (!objMsg) {
    return [];
  }
  if (!objMsg[prop]) {
    return [];
  }
  return objMsg[prop];
};

const feedbackClass = (message, prop, valid) => {
  const texts = getFeedback(message, prop, valid);
  let className = 'is-invalid';
  if (valid) {
    className = 'is-valid';
  }
  return texts.length > 0 ? className : '';
};

const renderList = (message, prop, valid) => {
  const texts = getFeedback(message, prop, valid);
  if (texts.length === 0) {
    return null;
  }
  const list = [];
  for (let i = 0; i < texts.length; i += 1) {
    list.push(<span key={`feedback-${prop}-${i}`}>{texts[i]}<br /></span>);
  }
  return list;
};

const renderContainer = (message, prop, valid) => {
  const texts = getFeedback(message, prop, valid);
  if (texts.length === 0) {
    return null;
  }
  return (
    <div className={`${valid ? 'valid' : 'invalid'}-feedback`}>
      {renderList(message, prop, valid)}
    </div>
  );
};

function Feedback({ message, prop, valid }) {
  return renderContainer(message, prop, valid);
}

Feedback.propTypes = {
  message: PropTypes.any,
  prop: PropTypes.string,
  valid: PropTypes.bool,
};

export default Feedback;

export {
  getFeedback,
  feedbackClass,
};
