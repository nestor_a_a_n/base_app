/**
 *
 * PrivateRoute
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Route, Redirect } from 'react-router-dom';

// Selectors
import { makeSelectDataUser } from '../../containers/App/selectors';

// Utils
import Auth from '../../utils/miscellaneous/auth';

const renderMergedProps = (component, ...rest) => {
  const finalProps = Object.assign({}, ...rest);
  return (
    React.createElement(component, finalProps)
  );
};

function PrivateRoute({ component: Component, dataUser, inverse, ...rest }) {
  return (
    Auth.isAuthenticated(dataUser, inverse)
    ? <Route {...rest} render={(routeProps) => renderMergedProps(Component, routeProps, rest)} />
    : <Redirect from={rest.path} to={`${Auth.redirect(inverse)}`} />
  );
}

PrivateRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  inverse: PropTypes.any,
  dataUser: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  dataUser: makeSelectDataUser(),
});

const withConnect = connect(
  mapStateToProps,
);

export default compose(
  withConnect,
)(PrivateRoute);
