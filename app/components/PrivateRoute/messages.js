/*
 * PrivateRoute Messages
 *
 * This contains all the text for the PrivateRoute component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.PrivateRoute.header',
    defaultMessage: 'This is the PrivateRoute component !',
  },
});
