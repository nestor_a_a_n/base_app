import 'whatwg-fetch';

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  if (response.status === 204 || response.status === 205) {
    return null;
  }
  return response.json();
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  if (response.status === 500 || response.status === 401) {
    return response;
  }
  // const error = new Error(response.statusText);
  const error = new Error(response.json());
  error.response = response;
  throw error;
}

/**
 * Requests a URL, returning a promise
 *
 * @param {string} endPoint
 * @param {object} [vars]
 *
 * @return {object}          The response data
 */
export default function request(configRequest, urlBaseApi) {
  // Traer la configuracion para el endpoint
  const { url, options } = configRequest;
  // Crear la url para el requerimiento
  const urlRequest = `${urlBaseApi}/${url}`;
  // Procesar el request
  return fetch(urlRequest, options)
    .then(checkStatus)
    .then(parseJSON);
}
