export default [
  {
    nameEnviroment: 'Local',
    key: 'local',
    urlApi: null, // Use internal api
    urlHost: 'http://localhost:3000',
    adapterApiQuery: 'adapterPhalconRest',
  },
  {
    nameEnviroment: 'Development',
    key: 'dev',
    urlApi: 'http://base_api.com',
    urlHost: 'http://localhost:3000',
    adapterApiQuery: 'adapterPhalconRest',
  },
];
