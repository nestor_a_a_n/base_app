import { defineMessages } from 'react-intl';

export default defineMessages({
  USERNAME: {
    id: 'modal.users.USERNAME',
    defaultMessage: 'Usuario',
  },
  DATE_REGISTERED: {
    id: 'modal.users.DATE_REGISTERED',
    defaultMessage: 'Fecha de registro',
  },
  ROLE: {
    id: 'modal.users.ROLE',
    defaultMessage: 'Rol',
  },
  STATUS: {
    id: 'modal.users.STATUS',
    defaultMessage: 'Estado',
  },
  USER_STATUS_CREATED: {
    id: 'modal.users.USER_STATUS_CREATED',
    defaultMessage: 'Creado',
  },
  USER_STATUS_ACTIVATED: {
    id: 'modal.users.USER_STATUS_ACTIVATED',
    defaultMessage: 'Activo',
  },
  USER_STATUS_BANNED: {
    id: 'modal.users.USER_STATUS_BANNED',
    defaultMessage: 'Baneado',
  },
  USER_STATUS_INACTIVE: {
    id: 'modal.users.USER_STATUS_INACTIVE',
    defaultMessage: 'Inactivo',
  },
  USER_ROLE_MEMBER: {
    id: 'modal.users.USER_ROLE_MEMBER',
    defaultMessage: 'Miembro',
  },
  USER_ROLE_STAFF: {
    id: 'modal.users.USER_ROLE_STAFF',
    defaultMessage: 'Personal',
  },
  USER_ROLE_OPERATOR: {
    id: 'modal.users.USER_ROLE_OPERATOR',
    defaultMessage: 'Operator',
  },
  USER_ROLE_MANAGER: {
    id: 'modal.users.USER_ROLE_MANAGER',
    defaultMessage: 'Gerente',
  },
  USER_ROLE_ADMINISTRATOR: {
    id: 'modal.users.USER_ROLE_ADMINISTRATOR',
    defaultMessage: 'Administrador',
  },
});
