import enviroments from './enviroments';
import data from './miscellaneous/data';
import sort from './miscellaneous/sort';

export default () => {
  sort();
  const nameEnv = 'enviroment';
  // Existe una configuracion ya almacenada?
  let eviroment = localStorage.getItem(nameEnv);
  if (!eviroment) {
    eviroment = enviroments[0];
  } else {
    eviroment = JSON.parse(eviroment);
  }
  localStorage.setItem(nameEnv, JSON.stringify(eviroment));
  Object.getOwnPropertyNames(eviroment).forEach((key) => {
    data[key] = eviroment[key];
  });
};
