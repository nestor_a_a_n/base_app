import { USER_ROLE_MEMBER, USER_ROLE_STAFF, USER_ROLE_OPERATOR, USER_ROLE_MANAGER, USER_ROLE_ADMINISTRATOR, USER_STATUS_CREATED, USER_STATUS_ACTIVATED, USER_STATUS_INACTIVE, USER_STATUS_BANNED } from '../../constants';

export default {
  roleUser: [USER_ROLE_MEMBER, USER_ROLE_STAFF, USER_ROLE_OPERATOR, USER_ROLE_MANAGER, USER_ROLE_ADMINISTRATOR],
  statusUser: [USER_STATUS_CREATED, USER_STATUS_ACTIVATED, USER_STATUS_INACTIVE, USER_STATUS_BANNED],
};
