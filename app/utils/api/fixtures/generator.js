import models from './models';
import { USER_ROLE_ADMINISTRATOR, USER_STATUS_ACTIVATED } from '../../constants';

export default {
  tables: {},
  get: (table) => {
    const tables = this.a.tables;
    if (!tables[table]) {
      tables[table] = [];
      let callGenerator = null;
      callGenerator = models[table];
      if (callGenerator !== null) {
        for (let i = 0; i < 100; i += 1) {
          const item = callGenerator.get();
          item.id = i;
          tables[table].push(item);
        }
        // Pos
        if (table === 'users') {
          tables[table][0] = {
            id: 0,
            username: 'demo',
            password: 'test123',
            role: USER_ROLE_ADMINISTRATOR,
            status: USER_STATUS_ACTIVATED,
            firstName: 'Demo',
            lastName: 'Demo',
            createAt: '2015',
            updateAt: null,
          };
        }
      }
    }
    return JSON.parse(JSON.stringify(tables[table]));
  },
};
