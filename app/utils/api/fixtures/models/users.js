import Fakerator from 'fakerator';
import moment from 'moment';
import mockdata from '../mockdata';
import { USER_ROLE_MEMBER, USER_STATUS_CREATED } from '../../../constants';

export default {
  get: () => {
    const fakerator = Fakerator();
    return {
      firstName: fakerator.names.firstName(),
      lastName: fakerator.names.lastName(),
      username: fakerator.internet.email(),
      password: fakerator.internet.password(8),
      role: fakerator.random.arrayElement(mockdata.roleUser),
      status: fakerator.random.arrayElement(mockdata.statusUser),
      createAt: moment(fakerator.date.past(10, new Date())).format('YYYY-MM-DD'),
      updateAt: moment(fakerator.date.recent(10)).format('YYYY-MM-DD'),
    };
  },
};

const schema = {
  firstName: {
    default: '',
    validators: {
      PresenceOf: {
        message: 'Los nombres son necesario',
      },
    },
  },
  lastName: {
    default: '',
    validators: {
      PresenceOf: {
        message: 'Los apellidos son necesario',
      },
    },
  },
  username: {
    // default: '',
    validators: {
      PresenceOf: {
        message: 'El correo es necesario',
      },
      Email: {
        message: 'Debe ser un correo',
      },
    },
  },
  password: {
    // default: '',
    validators: {
      PresenceOf: {
        message: 'El password es necesario',
      },
      StringLength: {
        min: 8,
        messageMinimum: 'Minimo 8 caracteres',
      },
      Callback: {
        callback: (data) => {
          if (typeof data.rePassword === 'undefined') {
            return true;
          }
          if (data.password !== data.rePassword) {
            return false;
          }
          return true;
        },
        message: 'No coinciden los passwords',
      },
    },
  },
  rePassword: {
    // default: '',
    validators: {
      PresenceOf: {
        message: 'Repetir password es necesario',
      },
      Callback: {
        callback: (data) => {
          if (data.password !== data.rePassword) {
            return false;
          }
          return true;
        },
        message: 'No coinciden los passwords',
      },
    },
  },
  role: {
    default: USER_ROLE_MEMBER,
  },
  status: {
    default: USER_STATUS_CREATED,
  },
};

export {
  schema,
};
