import endPointsFixture from './endPointsFixture';

export default {
  request: async (endPoint, vars = {}) => {
    await sleep(300);
    const proccess = endPointsFixture[endPoint];
    return proccess(vars);
  },
};

const sleep = (ms) => new Promise((resolve) => { setTimeout(resolve, ms); });
