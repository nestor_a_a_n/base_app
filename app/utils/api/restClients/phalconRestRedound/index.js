import endPoints from './endPoints';
import request from '../../../request';
import adapterQuery from './adapterQuery';

let urlBaseApi = null;
let auth = null;
let token = null;

const rest = {
  login: (vars) => request(endPoints.login(vars), urlBaseApi),
  list: (vars) => request(endPoints.list(vars, token), urlBaseApi),
  delete: (vars) => request(endPoints.delete(vars, token), urlBaseApi),
  setAuth: (payload) => {
    auth = payload;
    token = null;
    if (auth) {
      token = auth.token;
    }
  },
  setList: (list) => {
    adapterQuery.setList(list);
  },
  apiQuery: adapterQuery,
};

export default (baseUrl) => {
  urlBaseApi = baseUrl;
  return rest;
};
