import adaptarsQuery from './adapterPhalconRest';

export default {
  buildUrlQuery: (dataQuery, listConfig) => adaptarsQuery.buildUrlQuery(dataQuery, listConfig),
  getValueQuery: (dataQuery) => adaptarsQuery.getValueQuery(dataQuery),
  applyPaginate: (dataQuery) => adaptarsQuery.applyPaginate(dataQuery),
  applyFilters: (dataQuery) => adaptarsQuery.applyFilters(dataQuery),
  applySort: (dataQuery) => adaptarsQuery.applySort(dataQuery),
};
