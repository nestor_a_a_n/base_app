import { PHALCON_REST_PARAMETERS, PHALCON_REST_CONDITIONALS, PHALCON_REST_WHERE_IS_LESS_THAN_OR_EQUAL, PHALCON_REST_WHERE_IS_GREATER_THAN_OR_EQUAL, PHALCON_REST_WHERE_IS_LESS_THAN, PHALCON_REST_WHERE_IS_GREATER_THAN, PHALCON_REST_WHERE_IS_NOT_EQUAL, PHALCON_REST_WHERE_IS_EQUAL, PHALCON_REST_WHERE_LIKE, PHALCON_REST_WHERE, PHALCON_REST_SORT } from '../../../constants';
import { getObjectQuery, builUrlQuery as builUrlQueryBase } from '../../../miscellaneous/query';

/**
 * Contruye una url de consulta segun api Rest Phalcon
 * @param {Object} params
 * @return {String} Url
 */
const buildUrlQuery = (dataQuery, include) => {
  // { parameter, property, value, conditional }
  let actualData = {};
  try {
    actualData = JSON.parse(JSON.stringify(getObjectQuery()));
  } catch (e) {
    actualData = {};
  }
  if (include) {
    if (!actualData.include) {
      actualData.include = include;
    }
  }
  Object.getOwnPropertyNames(dataQuery).forEach((property) => {
    const { parameter, value, conditional } = dataQuery[property];
    if (PHALCON_REST_PARAMETERS.indexOf(parameter) === -1) {
      actualData[property] = value;
    } else {
      if (!actualData[parameter]) {
        actualData[parameter] = '';
      }
      let obj = {};
      if (actualData[parameter] !== '') {
        try {
          obj = JSON.parse(actualData[parameter]);
        } catch (e) {
          obj = {};
        }
      }
      if (!obj[property]) {
        obj[property] = {};
      }
      if (parameter === PHALCON_REST_WHERE) {
        if (PHALCON_REST_CONDITIONALS.indexOf(conditional) > -1) {
          obj[property][conditional] = String(value).trim();
          if (obj[property][conditional] === '') {
            delete obj[property][conditional];
          } else if (conditional === PHALCON_REST_WHERE_LIKE) {
            obj[property][conditional] = `%${obj[property][conditional]}%`;
          }
          if (JSON.stringify(obj[property]) === '{}') {
            delete obj[property];
          }
          if (JSON.stringify(obj) === '{}') {
            delete actualData[parameter];
          } else {
            actualData[parameter] = JSON.stringify(obj);
          }
        }
      } else if (parameter === PHALCON_REST_SORT) {
        obj[property] = String(value).trim();
        if (obj[property] === '') {
          delete obj[property];
        }
        if (JSON.stringify(obj) === '{}') {
          delete actualData[parameter];
        } else {
          actualData[parameter] = JSON.stringify(obj);
        }
      }
      // TODO
      // PHALCON_REST_FIELDS
      // PHALCON_REST_OFFSET
      // PHALCON_REST_LIMIT
      // PHALCON_REST_HAVING
      // PHALCON_REST_OR
      // actualData.page = 1;
    }
  });
  return builUrlQueryBase(actualData);
};

const getValueQuery = ({ parameter, property, conditional }) => {
  const actualData = JSON.parse(JSON.stringify(getObjectQuery()));
  if (actualData === '') {
    return '';
  }
  if (!actualData[parameter]) {
    return '';
  }
  try {
    actualData[parameter] = JSON.parse(actualData[parameter]);
  } catch (e) {
    return '';
  }
  if (parameter === PHALCON_REST_WHERE) {
    if (!actualData[parameter][property]) {
      return '';
    }
    if (!actualData[parameter][property][conditional]) {
      return '';
    }
    if (conditional === PHALCON_REST_WHERE_LIKE) {
      if (actualData[parameter][property][conditional].length > 0) {
        return actualData[parameter][property][conditional].substring(1, actualData[parameter][property][conditional].length - 1);
      }
    }
    return actualData[parameter][property][conditional];
  } else if (parameter === PHALCON_REST_SORT) {
    if (!actualData[parameter][property]) {
      return '';
    }
    return actualData[parameter][property];
  }
  return '';
};

const applyPaginate = ({ query, results, model }) => {
  const localQuery = getObjectQuery(query);
  if (typeof localQuery.page === 'undefined') {
    localQuery.page = 1;
  }
  if (typeof localQuery.limit === 'undefined') {
    localQuery.limit = 5;
  }
  const { page, limit } = localQuery;
  const newPage = parseInt(page, 10);
  const newLimit = parseInt(limit, 10);
  const list = [];
  for (let i = ((newPage - 1) * newLimit); i < (newPage * newLimit); i += 1) {
    if (typeof results[i] === 'object') {
      list.push(results[i]);
    }
  }
  const payload = {
    paginate: {
      totalItems: results.length,
      total: Math.ceil(results.length / newLimit),
      limit: newLimit,
      page: newPage,
    },
  };
  payload[model] = list;
  return payload;
};

const applyFilters = ({ query, results }) => {
  const localQuery = getObjectQuery(query);
  const properties = {};
  Object.getOwnPropertyNames(localQuery).forEach((parameter) => {
    if (PHALCON_REST_PARAMETERS.indexOf(parameter) > -1) {
      if (parameter === PHALCON_REST_WHERE) {
        let obj = {};
        try {
          obj = JSON.parse(localQuery[parameter]);
        } catch (e) {
          obj = {};
        }
        Object.getOwnPropertyNames(obj).forEach((property) => {
          if (typeof properties[property] === 'undefined') {
            properties[property] = {};
          }
          Object.getOwnPropertyNames(obj[property]).forEach((conditional) => {
            properties[property][conditional] = obj[property][conditional];
          });
        });
      }
    }
  });
  // TODO
  // PHALCON_REST_FIELDS
  // PHALCON_REST_OFFSET
  // PHALCON_REST_LIMIT
  // PHALCON_REST_HAVING
  // PHALCON_REST_OR
  if (JSON.stringify(properties) === '{}') {
    return results;
  }
  const newResults = [];
  for (let i = 0; i < results.length; i += 1) {
    const item = results[i];
    let add = true;
    Object.getOwnPropertyNames(properties).forEach((property) => {
      if (typeof item[property] === 'undefined') {
        newResults.push(item);
      } else {
        Object.getOwnPropertyNames(properties[property]).forEach((conditional) => {
          const valueCell = String(item[property]);
          const valueSearch = String(properties[property][conditional]);
          if (conditional === PHALCON_REST_WHERE_LIKE) {
            if (valueCell.indexOf(valueSearch) === -1) {
              add = false;
            }
          }
          if (conditional === PHALCON_REST_WHERE_IS_EQUAL) {
            if (valueCell !== valueSearch) {
              add = false;
            }
          }
          if (conditional === PHALCON_REST_WHERE_IS_NOT_EQUAL) {
            if (valueCell === valueSearch) {
              add = false;
            }
          }
          if (conditional === PHALCON_REST_WHERE_IS_GREATER_THAN) {
            if (valueCell <= valueSearch) {
              add = false;
            }
          }
          if (conditional === PHALCON_REST_WHERE_IS_LESS_THAN) {
            if (valueCell >= valueSearch) {
              add = false;
            }
          }
          if (conditional === PHALCON_REST_WHERE_IS_GREATER_THAN_OR_EQUAL) {
            if (valueCell < valueSearch) {
              add = false;
            }
          }
          if (conditional === PHALCON_REST_WHERE_IS_LESS_THAN_OR_EQUAL) {
            if (valueCell > valueSearch) {
              add = false;
            }
          }
        });
      }
    });
    if (add) {
      newResults.push(item);
    }
  }
  return newResults;
};

const applySort = ({ query, results }) => {
  const localQuery = getObjectQuery(query);
  const properties = {};
  Object.getOwnPropertyNames(localQuery).forEach((parameter) => {
    if (parameter === PHALCON_REST_SORT) {
      let obj = {};
      try {
        obj = JSON.parse(localQuery[parameter]);
      } catch (e) {
        obj = {};
      }
      Object.getOwnPropertyNames(obj).forEach((property) => {
        properties[property] = obj[property];
      });
    }
  });
  if (JSON.stringify(properties) === '{}') {
    return results;
  }
  let inverse = 0;
  results.sortBy((o) => {
    const list = [];
    Object.getOwnPropertyNames(properties).forEach((property) => {
      if (typeof o[property] === 'string') {
        inverse = properties[property];
        list.push(o[property]);
      } else if (typeof o[property] === 'number') {
        list.push(-properties[property] * o[property]);
      }
    });
    return list;
  });
  if (inverse === '-1') {
    const newList = [];
    for (let i = (results.length - 1); i >= 0; i -= 1) {
      newList.push(results[i]);
    }
    return newList;
  }
  return results;
};

export default {
  buildUrlQuery,
  getValueQuery,
  applyPaginate,
  applyFilters,
  applySort,
};
