import base64 from 'base-64';

export default ({ username, password }) => {
  const basic = `${username}:${password}`;
  return {
    url: 'users/authenticate',
    options: {
      method: 'POST',
      headers: {
        Authorization: `Basic ${base64.encode(basic)}`,
      },
    },
  };
};
