export default ({ source, id }, authToken) => (
  {
    url: `${source}/${id}`,
    options: {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
    },
  }
);
