export default ({ source, query }, authToken) => {
  const newQuery = query !== '' ? `?${query}` : query;
  return {
    url: `${source}${newQuery}`,
    options: {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
    },
  };
};
