import login from './login';
import list from './list';

export default {
  login,
  list,
};
