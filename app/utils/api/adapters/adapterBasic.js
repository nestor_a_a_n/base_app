import { getObjectQuery, builUrlQuery as builUrlQueryBase } from '../../miscellaneous/query';

const buildUrlQuery = (newData) => {
  const actualData = JSON.parse(JSON.stringify(getObjectQuery()));
  Object.getOwnPropertyNames(newData).forEach((key) => {
    actualData[key] = newData[key];
  });
  return builUrlQueryBase(actualData);
};

const getValueQuery = () => {

};

const applyPaginate = () => {

};

const applyFilters = () => {

};

const applySort = () => {

};

export default {
  buildUrlQuery,
  getValueQuery,
  applyPaginate,
  applyFilters,
  applySort,
};
