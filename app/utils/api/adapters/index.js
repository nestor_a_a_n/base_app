import adapterBasic from './adapterBasic';
import adapterPhalconRest from './adapterPhalconRest';

export default {
  adapterBasic,
  adapterPhalconRest,
};
