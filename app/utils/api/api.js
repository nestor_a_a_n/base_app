import base64 from 'base-64';

const usersAuthenticate = ({ username, password }) => {
  const basic = `${username}:${password}`;
  return {
    apiServer: null,
    url: 'users/authenticate',
    query: '',
    options: {
      Authorization: `Basic ${base64.encode(basic)}`,
    },
  };
};

export default {};

export {
  usersAuthenticate,
};

