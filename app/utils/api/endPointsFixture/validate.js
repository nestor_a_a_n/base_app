import listValidators from './validators/index';
import errorApi from './errorApi';

/* eslint no-param-reassign: [2, { "props": false }] */
export default (data, schemas, proccess) => {
  const errors = {};
  Object.getOwnPropertyNames(schemas).forEach((prop) => {
    if (typeof schemas[prop].default !== 'undefined') {
      data[prop] = schemas[prop].default;
    }
    if (schemas[prop].validators) {
      Object.getOwnPropertyNames(schemas[prop].validators).forEach((nameValidator) => {
        listValidators[nameValidator](data, prop, errors, schemas[prop].validators[nameValidator]);
      });
    }
  });
  if (errors.has) {
    delete errors.has;
    return errorApi(JSON.stringify(errors));
  }
  return proccess(data);
};
