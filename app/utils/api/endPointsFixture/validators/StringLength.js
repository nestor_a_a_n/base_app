import addError from '../addError';

export default (data, name, errors, config) => {
  if (typeof data[name] === 'undefined') {
    return;
  }
  if (config.min) {
    if (data[name].length < config.min) {
      addError(errors, name, config.messageMinimum);
    }
  }
  if (config.max) {
    if (data[name].length > config.max) {
      addError(errors, name, config.messageMaximum);
    }
  }
};
