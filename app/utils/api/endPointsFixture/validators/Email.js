import addError from '../addError';
import validateEmail from '../../../miscellaneous/validateEmail';

export default (data, name, errors, config) => {
  if (typeof data[name] === 'undefined') {
    return;
  }
  if (!validateEmail(data[name])) {
    addError(errors, name, config.message);
  }
};
