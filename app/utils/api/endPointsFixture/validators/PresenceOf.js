import addError from '../addError';

export default (data, prop, errors, config) => {
  if (typeof data[prop] === 'undefined') {
    addError(errors, prop, config.message);
  }
};
