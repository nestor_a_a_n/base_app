import Callback from './Callback';
import Email from './Email';
import PresenceOf from './PresenceOf';
import StringLength from './StringLength';

export default {
  Callback,
  Email,
  PresenceOf,
  StringLength,
};
