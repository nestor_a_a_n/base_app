import addError from '../addError';

export default (data, name, errors, config) => {
  if (typeof data[name] === 'undefined') {
    return;
  }
  if (!config.callback(data, name)) {
    addError(errors, name, config.message);
  }
};
