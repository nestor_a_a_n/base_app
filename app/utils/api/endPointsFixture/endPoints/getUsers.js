import generator from '../../fixtures/generator';
import apiQuery from '../../../miscellaneous/adapterQuery';

export default ({ query }) => {
  const model = 'users';
  let results = generator.get(model);
  results = apiQuery.applyFilters({ query, results });
  results = apiQuery.applySort({ query, results });
  return apiQuery.applyPaginate({ query, results, model });
};
