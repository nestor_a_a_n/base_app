import errorApi from '../errorApi';
import search from '../../../miscellaneous/search';
import generator from '../../fixtures/generator';

export default ({ username, password }) => {
  const users = generator.get('users');
  const user = search(users, 'username', username);
  if (user === null) {
    return errorApi('Email no existe');
  }
  if (user.username === username && user.password === password) {
    return {
      data: {
        token: 'sdasdasdasdasdas',
        expires: 13156464,
        user,
      },
    };
  }
  return errorApi('Authentication: Login Failed');
};
