import errorApi from '../errorApi';
import search from '../../../miscellaneous/search';
import generator from '../../fixtures/generator';

export default ({ username, code, password, rePassword }) => {
  if (password !== rePassword) {
    return errorApi('Las contraseña no coinciden');
  }
  if (password.length < 8) {
    return errorApi('Las contraseña minimo debe tener 8 caracteres');
  }
  const users = generator.get('users');
  const user = search(users, 'username', username);
  if (user === null) {
    return errorApi('Authentication: Email Failed');
  }
  if (user.code !== code) {
    return errorApi('Authentication: Email Failed');
  }
  user.password = password;
  user.code = '';
  return {
    user,
  };
};
