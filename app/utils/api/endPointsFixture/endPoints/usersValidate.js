import errorApi from '../errorApi';
import search from '../../../miscellaneous/search';
import generator from '../../fixtures/generator';

export default ({ username, code }) => {
  if (code === '') {
    return errorApi('Authentication: Validate Failed');
  }
  // Buscar el usuario
  const users = generator.get('users');
  const user = search(users, 'username', username);
  if (user === null) {
    return errorApi('Authentication: Validate Failed');
  }
  if (user.code !== code) {
    return errorApi('Authentication: Validate Failed');
  }
  return {
    data: {
      token: 'sdasdasdasdasdas',
      expires: 13156464,
      user,
    },
  };
};
