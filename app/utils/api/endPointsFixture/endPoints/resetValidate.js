import errorApi from '../errorApi';
import search from '../../../miscellaneous/search';
import generator from '../../fixtures/generator';

export default ({ username, code }) => {
  const users = generator.get('users');
  const user = search(users, 'username', username);
  if (user === null) {
    return errorApi('Authentication: Email Failed');
  }
  if (user.code !== code) {
    return errorApi('Authentication: Email Failed');
  }
  return {
    user,
  };
};
