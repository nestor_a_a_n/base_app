import generator from '../../fixtures/generator';
import makeid from '../../../miscellaneous/makeid';
import data from '../../../miscellaneous/data';
import validate from '../validate';
import { schema as schemaUsers } from '../../fixtures/models/users';

export default (dataApi) => validate(dataApi, schemaUsers, process);
// Buscar el usuario
// const users = generator.get('users');
// let user = search(users, 'username', username);
// if (user !== null) {
//   addError(error, 'username', 'El correo ya existe');
// }

const process = ({ username, password }) => {
  const users = generator.get('users');
  const user = users[0];
  const newUser = {};
  Object.getOwnPropertyNames(user).forEach((val) => {
    newUser[val] = '';
  });
  newUser.id = users.length + 1;
  newUser.username = username;
  newUser.password = password;
  newUser.code = makeid(5);
  data.codeValidate = newUser.code;
  users.push(newUser);
  return {
    user: newUser,
  };
};
