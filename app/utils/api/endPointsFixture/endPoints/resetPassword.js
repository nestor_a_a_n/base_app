import errorApi from '../errorApi';
import search from '../../../miscellaneous/search';
import makeid from '../../../miscellaneous/makeid';
import generator from '../../fixtures/generator';
import data from '../../../miscellaneous/data';

export default ({ username }) => {
  const users = generator.get('users');
  const user = search(users, 'username', username);
  if (user === null) {
    return errorApi('Authentication: Email Failed');
  }
  user.code = makeid(5);
  data.codeValidate = user.code;
  return {
    user,
  };
};
