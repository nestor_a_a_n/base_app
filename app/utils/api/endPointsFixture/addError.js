/* eslint no-param-reassign: ["error", { "props": false }] */
export default (obj, prop, text) => {
  if (typeof obj[prop] === 'undefined') {
    obj[prop] = [];
  }
  obj.has = true;
  obj[prop].push(text);
};
