import getUsers from './endPoints/getUsers';
import resetPassword from './endPoints/resetPassword';
import resetValidate from './endPoints/resetValidate';
import updatePassword from './endPoints/updatePassword';
import usersAuthenticate from './endPoints/usersAuthenticate';
import usersRegister from './endPoints/usersRegister';
import usersValidate from './endPoints/usersValidate';

export default {
  getUsers,
  resetPassword,
  resetValidate,
  updatePassword,
  usersAuthenticate,
  usersRegister,
  usersValidate,
};
