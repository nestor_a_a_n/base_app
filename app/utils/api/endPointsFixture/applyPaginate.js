import { getQuery } from '../../miscellaneous/query';

export default (query, results, model) => {
  const localQuery = getQuery(query);
  if (typeof localQuery.page === 'undefined') {
    localQuery.page = 1;
  }
  if (typeof localQuery.limit === 'undefined') {
    localQuery.limit = 5;
  }
  const { page, limit } = localQuery;
  const newPage = parseInt(page, 10);
  const newLimit = parseInt(limit, 10);
  const list = [];
  for (let i = ((newPage - 1) * newLimit); i < (newPage * newLimit); i += 1) {
    if (typeof results[i] === 'object') {
      list.push(results[i]);
    }
  }
  const payload = {
    paginate: {
      total_items: results.length,
      total: Math.ceil(results.length / newLimit),
      limit: newLimit,
      page: newPage,
    },
  };
  payload[model] = list;
  return payload;
};
