import { getQuery } from '../../miscellaneous/query';
import { PHALCON_REST_PARAMETERS, PHALCON_REST_WHERE, PHALCON_REST_WHERE_LIKE, PHALCON_REST_WHERE_IS_EQUAL, PHALCON_REST_WHERE_IS_NOT_EQUAL, PHALCON_REST_WHERE_IS_GREATER_THAN, PHALCON_REST_WHERE_IS_LESS_THAN, PHALCON_REST_WHERE_IS_GREATER_THAN_OR_EQUAL, PHALCON_REST_WHERE_IS_LESS_THAN_OR_EQUAL } from '../../constants';

export default ({ query, results }) => {
  const localQuery = getQuery(query);
  const properties = {};
  Object.getOwnPropertyNames(localQuery).forEach((parameter) => {
    if (PHALCON_REST_PARAMETERS.indexOf(parameter) > -1) {
      if (parameter === PHALCON_REST_WHERE) {
        let obj = {};
        try {
          obj = JSON.parse(localQuery[parameter]);
        } catch (e) {
          obj = {};
        }
        Object.getOwnPropertyNames(obj).forEach((property) => {
          if (typeof properties[property] === 'undefined') {
            properties[property] = {};
          }
          Object.getOwnPropertyNames(obj[property]).forEach((conditional) => {
            properties[property][conditional] = obj[property][conditional];
          });
        });
      }
    }
  });
  // TODO
  // PHALCON_REST_FIELDS
  // PHALCON_REST_OFFSET
  // PHALCON_REST_LIMIT
  // PHALCON_REST_HAVING
  // PHALCON_REST_OR
  if (JSON.stringify(properties) === '{}') {
    return results;
  }
  const newResults = [];
  for (let i = 0; i < results.length; i += 1) {
    const item = results[i];
    let add = true;
    Object.getOwnPropertyNames(properties).forEach((property) => {
      if (typeof item[property] === 'undefined') {
        newResults.push(item);
      } else {
        Object.getOwnPropertyNames(properties[property]).forEach((conditional) => {
          const valueCell = String(item[property]);
          const valueSearch = String(properties[property][conditional]);
          if (conditional === PHALCON_REST_WHERE_LIKE) {
            if (valueCell.indexOf(valueSearch) === -1) {
              add = false;
            }
          }
          if (conditional === PHALCON_REST_WHERE_IS_EQUAL) {
            if (valueCell !== valueSearch) {
              add = false;
            }
          }
          if (conditional === PHALCON_REST_WHERE_IS_NOT_EQUAL) {
            if (valueCell === valueSearch) {
              add = false;
            }
          }
          if (conditional === PHALCON_REST_WHERE_IS_GREATER_THAN) {
            if (valueCell <= valueSearch) {
              add = false;
            }
          }
          if (conditional === PHALCON_REST_WHERE_IS_LESS_THAN) {
            if (valueCell >= valueSearch) {
              add = false;
            }
          }
          if (conditional === PHALCON_REST_WHERE_IS_GREATER_THAN_OR_EQUAL) {
            if (valueCell < valueSearch) {
              add = false;
            }
          }
          if (conditional === PHALCON_REST_WHERE_IS_LESS_THAN_OR_EQUAL) {
            if (valueCell > valueSearch) {
              add = false;
            }
          }
        });
      }
    });
    if (add) {
      newResults.push(item);
    }
  }
  return newResults;
};
