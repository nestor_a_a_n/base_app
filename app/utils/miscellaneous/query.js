import data from './data';

/**
 * Retornar la url has como objeto JSON
 * @return {Object} Parametros de la url y sus valores
 */
const getObjectQuery = () => {
  if (data.lastHash !== window.location.hash) {
    data.lastHash = window.location.hash;
    data.lastDataHash = transformQuery(window.location.hash);
  }
  return data.lastDataHash;
};

/**
 * Transforma la url en un objeto json
 * @param {String} query Url de hahs
 * @return {Object} Parametros de ls url
 */
const transformQuery = (query) => {
  let newQuery = query.replace('?', '');
  newQuery = newQuery.replace('#', '');
  const arrayQuery = newQuery.split('&');
  const resultQuery = {};
  for (let i = 0; i < arrayQuery.length; i += 1) {
    const vars = arrayQuery[i].split('=');
    if (vars.length === 2) {
      resultQuery[vars[0]] = vars[1];
    }
  }
  return resultQuery;
};

/**
 * De la actual url le agrega nuevos parametros
 * @param {Object} actualData Datos a actualizar de la url
 * @return {String} Nueva url
 */
const builUrlQuery = (actualData) => {
  let query = '';
  Object.getOwnPropertyNames(actualData).forEach((key) => {
    const obj = actualData;
    if (query === '') {
      query = `${key}=${obj[key]}`;
    } else {
      query = `${query}&${key}=${obj[key]}`;
    }
  });
  return query;
};

export {
  transformQuery,
  getObjectQuery,
  builUrlQuery,
};
