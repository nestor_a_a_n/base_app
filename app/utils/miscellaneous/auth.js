import { URL_BASE, URL_LOGIN } from '../constants';

export default {
  isAuthenticated(dataUser, inverse) {
    // const dataUser = store.get(DATA_USER, {});
    if (typeof inverse === 'undefined') {
      return !!dataUser.user;
    }
    return !dataUser.user;
  },
  redirect(inverse) {
    if (typeof inverse === 'undefined') {
      return URL_LOGIN;
    }
    return URL_BASE;
  },
};
