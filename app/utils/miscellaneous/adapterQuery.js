import data from './data';
import adaptarsQuery from '../api/adapters';

export default {
  buildUrlQuery: (dataQuery) => adaptarsQuery[data.adapterApiQuery].buildUrlQuery(dataQuery),
  getValueQuery: (dataQuery) => adaptarsQuery[data.adapterApiQuery].getValueQuery(dataQuery),
  applyPaginate: (dataQuery) => adaptarsQuery[data.adapterApiQuery].applyPaginate(dataQuery),
  applyFilters: (dataQuery) => adaptarsQuery[data.adapterApiQuery].applyFilters(dataQuery),
  applySort: (dataQuery) => adaptarsQuery[data.adapterApiQuery].applySort(dataQuery),
};
