export default {
  next: (evt, ref) => {
    if (!ref) {
      return;
    }
    if (typeof ref.focus !== 'function') {
      return;
    }
    const keyCode = evt.keyCode || evt.which;
    if (evt.shiftKey === false && keyCode === 13) {
      evt.preventDefault();
      ref.focus();
    }
  },
  submit: (evt, callback) => {
    if (typeof callback !== 'function') {
      return;
    }
    const keyCode = evt.keyCode || evt.which;
    if (evt.shiftKey === false && keyCode === 13) {
      evt.preventDefault();
      callback();
    }
  },
};
