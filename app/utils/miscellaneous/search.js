export default (list, prop, value) => {
  for (let i = 0; i < list.length; i += 1) {
    const element = list[i];
    if (element[prop] === value) {
      return element;
    }
  }
  return null;
};
