/* eslint-disable */

export default () => {
  if (typeof Object.defineProperty === 'function') {
    try {
      /* eslint no-extend-native: ["error", { "exceptions": ["Object"] }] */
      Object.defineProperty(Array.prototype, 'sortBy', { value: sortBy });
    } catch (e) {
      Array.prototype.sortBy = null;
    }
  }
  if (!Array.prototype.sortBy) {
    /* eslint no-extend-native: ["error", { "exceptions": ["Object"] }] */
    Array.prototype.sortBy = sortBy;
  }

  const DESCSORT = (/^desc:\s*/i);

  // comparison criteria
  function comparerSort(prev, next) {
    let asc = 1;
    if (typeof prev === 'string') {
      if (DESCSORT.test(prev)) asc = -1;
    }
    if (prev === next) return 0;
    return (prev > next ? 1 : -1) * asc;
  }

  function sortBy(parser) {
    let i;
    let item;
    const arrLength = this.length;
    if (typeof parser !== 'function') {
      return this.sort(Array.prototype.sort.bind(this));
    }
    // Schwartzian transform (decorate-sort-undecorate)
    for (i = arrLength; i;) {
      item = this[i -= 1];
      // decorate the array
      this[i] = [].concat(parser.call(null, item, i), item);
    }
    // sort the array
    this.sort((prev, next) => {
      let sorted;
      const length = prev.length;
      for (i = 0; i < length; i += 1) {
        sorted = comparerSort(prev[i], next[i]);
        if (sorted) return sorted;
      }
      return 0;
    });
    // undecorate the array
    for (i = arrLength; i;) {
      item = this[i -= 1];
      this[i] = item[item.length - 1];
    }
    return this;
  }
};
