import data from './data';
import injectEnviroment from '../injectEnviroment';

export default {
  set(key, value) {
    if (!data.key) { injectEnviroment(); }
    localStorage.setItem(`${data.key}_${key}`, JSON.stringify(value));
  },
  get(key, defaultValue) {
    if (!data.key) { injectEnviroment(); }
    const value = localStorage.getItem(`${data.key}_${key}`);
    if (value === null) {
      return defaultValue;
    }
    return JSON.parse(value);
  },
};
