export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';

export const DATA_USER = 'dataUser';

// Urls
export const URL_LOGIN = '/login';
export const URL_RESET = '/reset';
export const URL_RESET_VALIDATE = '/reset-validate';
export const URL_REGISTER = '/register';
export const URL_REGISTER_VALIDATE = '/register-validate';
export const URL_404 = '/404';
export const URL_500 = '/500';
export const URL_BASE = '/';

export const URL_TEMPLATE = '/template';
export const URL_TEMPLATE_LOGIN = '/template/pages/login';
export const URL_TEMPLATE_404 = '/template/pages/404';
export const URL_TEMPLATE_500 = '/template/pages/500';
export const URL_TEMPLATE_REGISTER = '/template/pages/register';

// User Role
export const USER_ROLE_MEMBER = 1;
export const USER_ROLE_STAFF = 2;
export const USER_ROLE_OPERATOR = 3;
export const USER_ROLE_MANAGER = 4;
export const USER_ROLE_ADMINISTRATOR = 5;
export const USER_ROLE = {
  1: 'USER_ROLE_MEMBER',
  2: 'USER_ROLE_STAFF',
  3: 'USER_ROLE_OPERATOR',
  4: 'USER_ROLE_MANAGER',
  5: 'USER_ROLE_ADMINISTRATOR',
};
// User Status
export const USER_STATUS_CREATED = 0;
export const USER_STATUS_ACTIVATED = 1;
export const USER_STATUS_BANNED = 2;
export const USER_STATUS_INACTIVE = 3;
export const USER_STATUS = {
  0: {
    color: 'secondary',
    name: 'USER_STATUS_CREATED',
  },
  1: {
    color: 'success',
    name: 'USER_STATUS_ACTIVATED',
  },
  2: {
    color: 'danger',
    name: 'USER_STATUS_BANNED',
  },
  3: {
    color: 'warning',
    name: 'USER_STATUS_INACTIVE',
  },
};

// Phalcon Rest Query Parameters
export const PHALCON_REST_FIELDS = 'fields';
export const PHALCON_REST_OFFSET = 'offset';
export const PHALCON_REST_LIMIT = 'limit';
export const PHALCON_REST_HAVING = 'having';
export const PHALCON_REST_WHERE = 'where';
export const PHALCON_REST_OR = 'or';
export const PHALCON_REST_SORT = 'sort';
export const PHALCON_REST_PARAMETERS = [PHALCON_REST_FIELDS, PHALCON_REST_OFFSET, PHALCON_REST_LIMIT, PHALCON_REST_HAVING, PHALCON_REST_WHERE, PHALCON_REST_OR, PHALCON_REST_SORT];

// Phalcon Rest Query conditionals
export const PHALCON_REST_WHERE_LIKE = 'l';
export const PHALCON_REST_WHERE_IS_EQUAL = 'e';
export const PHALCON_REST_WHERE_IS_NOT_EQUAL = 'ne';
export const PHALCON_REST_WHERE_IS_GREATER_THAN = 'gt';
export const PHALCON_REST_WHERE_IS_LESS_THAN = 'lt';
export const PHALCON_REST_WHERE_IS_GREATER_THAN_OR_EQUAL = 'gte';
export const PHALCON_REST_WHERE_IS_LESS_THAN_OR_EQUAL = 'lte';
export const PHALCON_REST_CONDITIONALS = [PHALCON_REST_WHERE_LIKE, PHALCON_REST_WHERE_IS_EQUAL, PHALCON_REST_WHERE_IS_NOT_EQUAL, PHALCON_REST_WHERE_IS_GREATER_THAN, PHALCON_REST_WHERE_IS_LESS_THAN, PHALCON_REST_WHERE_IS_GREATER_THAN_OR_EQUAL, PHALCON_REST_WHERE_IS_LESS_THAN_OR_EQUAL];

// Tags Table
export const TABLE_TAG_INPUT = 'input';
export const TABLE_TAG_SELECT = 'select';
