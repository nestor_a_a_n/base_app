import React from 'react';
import { Badge } from 'reactstrap';
import phalconRestRedound from './utils/api/restClients/phalconRestRedound';
import { PHALCON_REST_WHERE, PHALCON_REST_WHERE_LIKE, PHALCON_REST_WHERE_IS_EQUAL, TABLE_TAG_INPUT, TABLE_TAG_SELECT } from './utils/constants';

export default {
  restClient: phalconRestRedound('http://base-api.com'),
  auth: false,
  resources: [
    {
      name: 'users',
      list: {
        icon: 'fa fa-align-justify',
        title: 'Users',
        types: {
          hover: true,
          bordered: true,
          striped: false,
          responsive: true,
          size: 'sm',
        },
        search: true,
        sort: true,
        sources: [
          {
            key: 'id',
          },
          {
            name: 'Username',
            key: 'username',
            search: {
              tag: 'input',
            },
          },
          {
            key: 'role',
          },
        ],
      },
      create: {},
      update: {},
      remove: {},
    },
    {
      name: 'photos',
      list: {
        sort: true,
        search: true,
        include: 'album',
        sources: [
          {
            key: 'id',
          },
          {
            key: 'title',
            search: {
              tag: TABLE_TAG_INPUT, // input, select
              parameter: PHALCON_REST_WHERE, // Parameter Query Phalcon Rest
              conditional: PHALCON_REST_WHERE_LIKE, // Conditional Query Phalcon Rest
            },
          },
          {
            key: 'albumId',
            search: {
              tag: TABLE_TAG_SELECT, // input, select
              parameter: PHALCON_REST_WHERE, // Parameter Query Phalcon Rest
              conditional: PHALCON_REST_WHERE_IS_EQUAL, // Conditional Query Phalcon Rest
              options: [{ text: '1', value: '1' }, { text: '2', value: '2' }],
              textField: 'text',
              valueField: 'value',
            },
          },
          {
            key: 'album',
            render: (cell) => cell.title,
          },
          {
            key: 'createdAt',
          },
          {
            key: 'updatedAt',
          },
        ],
      },
      create: {},
      update: {},
      remove: {},
    },
    {
      name: 'albums',
      icon: 'icon-star',
      list: {
        sort: true,
        include: 'photos',
        sources: [
          {
            key: 'id',
          },
          {
            key: 'title',
          },
          {
            key: 'photos',
            render: (cell) => cell.map((photo) => <Badge key={`${cell.key}-${photo.id}`} color="secondary">{photo.title}</Badge>),
          },
        ],
      },
    },
  ],
};
